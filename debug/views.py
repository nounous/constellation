from django.core.management import call_command
from django.http import HttpResponse
from django.views import View


class GraphModelsView(View):
    def get(self, request, *args, **kwargs):
        args = list(request.GET) or ['-a']
        call_command('graph_models', '-o', '/tmp/graph.svg', *args)
        return HttpResponse(open('/tmp/graph.svg', 'rb'), content_type='image/svg+xml')
