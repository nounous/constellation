from django.urls import path

from . import views


app_name = 'debug'
urlpatterns = [
    path('graph/', views.GraphModelsView.as_view(), name='graph_models'),
]
