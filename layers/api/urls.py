from rest_framework.routers import BaseRouter

from . import views


def register_routes(router: BaseRouter) -> None:
    router.register('layer/interface', views.InterfaceViewSet)
    router.register('layer/machine', views.MachineViewSet)
    router.register('layer/subnet', views.SubnetViewSet)
    router.register('layer/vlan', views.VlanViewSet)
