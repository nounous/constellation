import ipaddress

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from .layer2 import MACAddressField, Vlan
from .layer3 import Subnet


class VlanSubnets(models.Model):
    vlan = models.OneToOneField(
        Vlan,
        on_delete=models.CASCADE,
        related_name='subnets',
        verbose_name=_("VLAN"),
    )
    subnets = models.ManyToManyField(
        Subnet,
        related_name='vlans',
        verbose_name=_("IP subnets"),
    )

    def __str__(self):
        return _("Subnets on {}").format(self.vlan.name)

    class Meta:
        verbose_name = _("VLAN subnet")
        verbose_name_plural = _("VLAN subnets")


class IPAddress(models.Model):
    subnet = models.ForeignKey(
        Subnet,
        on_delete=models.PROTECT,
        related_name='ip_addresses',
        verbose_name=_("IP subnet"),
    )
    address = models.GenericIPAddressField(
        protocol='both',
        unique=True,
        verbose_name=_("IP address"),
    )
    interface = models.ForeignKey(
        'Interface',
        on_delete=models.CASCADE,
        related_name='ip_addresses',
        verbose_name=_("interface")
    )

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    def clean(self):
        network = self.subnet.ip_network()
        if ipaddress.ip_address(self.address) not in network:
            raise ValidationError(_("IP address must be in {subnet}.").format(subnet=network))

    def __str__(self):
        return f"{self.address}/{self.subnet.length}"

    class Meta:
        verbose_name = _("IP address")
        verbose_name_plural = _("IP addresses")


class Machine(models.Model):
    name = models.SlugField(
        unique=True,
        verbose_name=_("name"),
    )
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        related_name='machines',
        verbose_name=_("owner"),
    )
    description = models.TextField(
        blank=True,
        verbose_name=_("description"),
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("machine")
        verbose_name_plural = _("machines")
        indexes = [models.Index(fields=['owner'])]


class Interface(models.Model):
    machine = models.ForeignKey(
        Machine,
        on_delete=models.CASCADE,
        related_name='interfaces',
        verbose_name=_("machine"),
    )
    mac_address = MACAddressField(
        unique=True,
        verbose_name=_("MAC address"),
    )
    subnets = models.ManyToManyField(
        Subnet,
        related_name='+',
        through=IPAddress,
        verbose_name=_("subnets")
    )

    def owner(self):
        return self.machine.owner

    def __str__(self):
        return f'{self.mac_address}@{self.machine}'
