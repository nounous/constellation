import ipaddress

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _


class Subnet(models.Model):
    name = models.SlugField(
        unique=True,
        verbose_name=_("name"),
    )
    prefix = models.GenericIPAddressField(
        protocol='both',
        verbose_name=_("prefix"),
    )
    length = models.PositiveSmallIntegerField(
        verbose_name=_("length"),
    )

    def ip_network(self):
        return ipaddress.ip_network(f'{self.prefix}/{self.length}')

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    def clean(self):
        try:
            self.ip_network()
        except ValueError:
            raise ValidationError(_("Invalid IP subnet."))

    class Meta:
        verbose_name = _("subnet")
        verbose_name_plural = _("subnets")
        unique_together = ('prefix', 'length')
