"""constellation URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import importlib

from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path
from django.views.generic import TemplateView

urlpatterns = [
    path('', TemplateView.as_view(template_name='index.html'), name='index'),
    path('admin/', admin.site.urls, name='admin'),
    path('admin/doc/', include('django.contrib.admindocs.urls'), name='admindocs'),
    path('accounts/', include('django.contrib.auth.urls')),

    path('i18n/', include('django.conf.urls.i18n')),
]

# Load urls from other apps if they are loaded
for app in settings.INSTALLED_APPS:
    if app.startswith("django"):
        continue
    try:
        urlpatterns.append(path(f'{app}/', include(app + '.urls')))
    except ModuleNotFoundError:
        pass


# If Django Rest Framework is installed, then add a REST API
try:
    from rest_framework import routers
except ImportError:
    pass
else:
    router = routers.DefaultRouter()

    for app in settings.LOCAL_APPS:
        try:
            api_urls = importlib.import_module(f'{app}.api.urls')
            # noinspection PyUnresolvedReferences
            api_urls.register_routes(router)
        except ModuleNotFoundError:
            pass

    if 'dnsmanager' in settings.INSTALLED_APPS:
        from dnsmanager.api.views import RecordViewSet, ZoneViewSet
        router.register('dnsmanager/record', RecordViewSet)
        router.register('dnsmanager/zone', ZoneViewSet)

    urlpatterns += [
        url(r'^api/', include(router.urls)),
    ]
