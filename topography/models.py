from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


class BuildingGroup(models.Model):
    name = models.SlugField(
        verbose_name=_("name"),
    )
    full_name = models.CharField(
        max_length=256,
        blank=True,
        verbose_name=_("full name"),
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("building group")
        verbose_name_plural = _("building groups")


class Building(models.Model):
    group = models.ForeignKey(
        BuildingGroup,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='buildings',
        verbose_name=_("group"),
    )
    number = models.SlugField(
        verbose_name=_("number"),
    )
    name = models.CharField(
        max_length=256,
        verbose_name=_("name"),
    )
    address = models.CharField(
        max_length=256,
        verbose_name=_("address"),
    )
    latitude = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("latitude"),
    )
    longitude = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("longitude"),
    )

    def __str__(self):
        return f'{self.number}' if self.group is None else f'{self.number}@{self.group.name}'

    class Meta:
        verbose_name = _("building")
        verbose_name_plural = _("buildings")
        unique_together = ('group', 'number')


class Room(models.Model):
    building = models.ForeignKey(
        Building,
        on_delete=models.CASCADE,
        related_name='rooms',
        verbose_name=_("building"),
    )
    number = models.SlugField(
        verbose_name=_("number"),
    )
    occupant = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='room',
        verbose_name=_("occupant"),
    )

    def __str__(self):
        return f'{self.building.number}{self.number}' if self.building.group is None \
            else f'{self.building.number}{self.number}@{self.building.group.name}'

    class Meta:
        verbose_name = _("room")
        verbose_name_plural = _("rooms")
        unique_together = ('building', 'number')
