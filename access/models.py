from django.db import models
from django.utils.translation import gettext_lazy as _
from layers.models import Interface, Machine


class WifiInterface(Interface):
    username = models.SlugField(
        verbose_name=_("username"),
    )
    password = models.CharField(
        max_length=128,
        verbose_name=_("password"),
    )

    class Meta:
        verbose_name = _("WIFI interface")
        verbose_name_plural = _("WIFI interfaces")


class AccessPoint(Machine):
    location = models.CharField(
        max_length=256,
        verbose_name=_("location"),
    )
    latitude = models.FloatField(
        verbose_name=_("latitude"),
    )
    longitude = models.FloatField(
        verbose_name=_("longitude"),
    )

    class Meta:
        verbose_name = _("access point")
        verbose_name_plural = _("access points")


class Switch(Machine):
    building = models.ForeignKey(
        'topography.Building',
        on_delete=models.PROTECT,
        verbose_name=_("building"),
    )
    location = models.CharField(
        max_length=256,
        verbose_name=_("location"),
    )
    model = models.CharField(
        max_length=32,
        verbose_name=_("model"),
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("switch")
        verbose_name_plural = _("switches")


class Port(models.Model):
    switch = models.ForeignKey(
        'access.Switch',
        on_delete=models.CASCADE,
        related_name='ports',
        verbose_name=_("switch"),
    )
    module = models.SlugField(
        blank=True,
        verbose_name=_("module"),
    )
    number = models.PositiveSmallIntegerField(
        verbose_name=_("number"),
    )
    vlans = models.ManyToManyField(
        'layers.Vlan',
        verbose_name=_("VLANs")
    )
    room = models.ForeignKey(
        'topography.Room',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='switch_ports',
        verbose_name=_("room"),
    )
    port = models.ForeignKey(
        'self',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name=_("port"),
    )
    interface = models.ForeignKey(
        'layers.Interface',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='switch_port',
        verbose_name=_("interface"),
    )

    def __str__(self):
        return f'{self.module}{self.number}@{self.switch.name}'

    class Meta:
        verbose_name = _("port")
        verbose_name_plural = _("ports")
        unique_together = ('switch', 'module', 'number')
