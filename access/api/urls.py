from rest_framework.routers import BaseRouter

from . import views


def register_routes(router: BaseRouter) -> None:
    router.register('access/switch', views.SwitchViewSet)
