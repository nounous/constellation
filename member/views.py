from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import transaction
from django.http import Http404
from django.shortcuts import redirect, resolve_url
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import gettext_lazy as _
from django.views import View
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import CreateView, DetailView, UpdateView, RedirectView, TemplateView

from . import forms
from .models import Person
from .tokens import email_validation_token


@method_decorator(sensitive_post_parameters('password1', 'password2'), 'dispatch')
class SignupView(CreateView):
    model = User
    form_class = forms.SignupForm
    template_name = 'member/signup.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['member_form'] = forms.PersonForm(self.request.POST or None)
        return context

    @transaction.atomic
    def form_valid(self, form):
        member_form = forms.PersonForm(self.request.POST or None)
        if not member_form.is_valid():
            return self.form_invalid(form)

        ret = super().form_valid(form)

        member = member_form.instance
        member.user = form.instance
        member.save()

        return ret

    def get_success_url(self):
        return reverse_lazy('member:user_detail', args=(self.object.pk,))


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    context_object_name = 'user_object'
    template_name = 'member/user_detail.html'


class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    form_class = forms.UserForm
    context_object_name = 'user_object'
    template_name = 'member/user_update.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.get_object()
        profile_form_class = forms.PersonForm if isinstance(user.profile, Person) else forms.OrganizationForm
        context['profile_form'] = profile_form_class(data=self.request.POST or None, instance=user.profile)
        return context

    @transaction.atomic
    def form_valid(self, form):
        user = form.instance
        profile_form_class = forms.PersonForm if isinstance(user.profile, Person) else forms.OrganizationForm
        profile_form = profile_form_class(data=self.request.POST or None, instance=user.profile)
        if not profile_form.is_valid():
            return self.form_invalid(form)

        ret = super().form_valid(form)

        profile_form.save()

        return ret

    def get_success_url(self):
        return reverse_lazy('member:user_detail', args=(self.object.pk,))


class UserValidateView(TemplateView):
    """
    A view to validate the email address.
    """
    template_name = 'member/email_validation_complete.html'

    def get(self, *args, **kwargs):
        """
        With a given token and user id (in params), validate the email address.
        """
        assert 'uidb64' in kwargs and 'token' in kwargs

        self.validlink = False
        user = self.get_user(kwargs['uidb64'])
        token = kwargs['token']

        # Validate the token
        if user is not None and email_validation_token.check_token(user, token):
            # The user must wait that someone validates the account before the user can be active and login.
            self.validlink = True
            user.profile.email_confirmed = True
            user.save()
            user.profile.save()
        return self.render_to_response(self.get_context_data(), status=200 if self.validlink else 400)

    def get_user(self, uidb64):
        """
        Get user from the base64-encoded string.
        """
        try:
            # urlsafe_base64_decode() decodes to bytestring
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist, ValidationError):
            user = None
        return user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user_object'] = self.get_user(self.kwargs["uidb64"])
        context['login_url'] = resolve_url(settings.LOGIN_URL)
        if self.validlink:
            context['validlink'] = True
        else:
            context.update({
                'title': _('Email validation unsuccessful'),
                'validlink': False,
            })
        return context


class UserResendValidationEmailView(LoginRequiredMixin, DetailView):
    """
    Resend the email validation link.
    """
    model = User

    def get(self, request, *args, **kwargs):
        user = self.get_object()

        if user != request.user and not request.user.has_perm('member.change_profile'):
            raise Http404

        user.profile.send_email_validation_link()
        messages.info(request, _("Email confirmation link was successfully sent"))

        return redirect('member:user_detail', user.id)


class UserImpersonateView(LoginRequiredMixin, RedirectView):
    """
    An administrator can log in through this page as someone else,
    and act as this other person.
    """

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_superuser:
            if not User.objects.filter(pk=kwargs["pk"]).exists():
                raise Http404
            session = request.session
            session["admin"] = request.user.pk
            session["_fake_user_id"] = kwargs["pk"]
        return super().dispatch(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        return reverse_lazy("member:user_detail", args=(kwargs["pk"],))


class ResetAdminView(LoginRequiredMixin, View):
    """
    Return to admin view, clear the session field
    that let an administrator to log in as someone else.
    """

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if not user.is_authenticated:
            return self.handle_no_permission()
        if "_fake_user_id" in request.session:
            del request.session["_fake_user_id"]
        return redirect(request.GET.get("path", reverse_lazy("index")))
