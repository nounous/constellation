from django.apps import AppConfig
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import MaxLengthValidator, RegexValidator
from django.db.models import Q
from django.db.models.signals import post_save
from django.utils.translation import gettext_lazy as _


class MemberConfig(AppConfig):
    name = 'member'
    verbose_name = _("member")

    def ready(self):
        from . import signals
        post_save.connect(signals.create_superuser_profile, settings.AUTH_USER_MODEL)
        post_save.connect(signals.ensure_email_not_empty, settings.AUTH_USER_MODEL)
        post_save.connect(signals.send_email_address_confirmation, 'member.Profile')
        post_save.connect(signals.send_email_address_confirmation, 'member.Person')
        post_save.connect(signals.send_email_address_confirmation, 'member.Organization')

        # We override default Django validators for the user to add our custom validators.
        # We don't replace the User because this needs an app, and in order to keep modularity,
        # we don't want to include an application that is required everywhere.
        # By that way, forms will perform good verifications and Django will prevent fields to be
        # invalid. However, database is not protected.

        from django.contrib.auth.models import User
        User._meta.get_field('username').validators = [
            MaxLengthValidator(32),
            RegexValidator('^[a-z][a-z0-9-]+$',
                           message=_("A username must contain only lowercase letters, digits and dashes.")),
        ]

        def clean_fields(user, exclude=False):
            if not user.email:
                raise ValidationError({'email': _("A valid email address is required.")})
            return super(User, user).clean_fields(exclude)
        User.clean_fields = clean_fields

        def validate_unique(user, exclude=False):
            if User.objects.filter(~Q(pk=user.pk) & Q(email=user.email)).exists():
                raise ValidationError({'email': _("This email address is already used. "
                                                  "Please login or reset your password to access to your account.")})
            return super(User, user).validate_unique(exclude)
        User.validate_unique = validate_unique
