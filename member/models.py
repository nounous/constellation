from django.conf import settings
from django.contrib.sites.models import Site
from django.db import models
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import gettext_lazy as _
from polymorphic.models import PolymorphicModel
from django.urls import reverse_lazy

from datetime import datetime

from member.tokens import email_validation_token


class Profile(PolymorphicModel):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='profile',
        verbose_name=_("user"),
    )

    address = models.CharField(
        max_length=256,
        verbose_name=_("address"),
    )

    email_confirmed = models.BooleanField(
        default=False,
        verbose_name=_("email address confirmed"),
    )

    @property
    def is_person(self):  # pragma: no cover
        raise NotImplementedError

    @property
    def has_current_membership(self):  # pragma: no cover
        raise NotImplementedError

    @property
    def current_membership(self):  # pragma: no cover
        raise NotImplementedError

    @property
    def is_organization(self):
        return not self.is_person

    def send_email_validation_link(self):
        subject = "[Crans] " + str(_("Activate your account"))
        token = email_validation_token.make_token(self.user)
        uid = urlsafe_base64_encode(force_bytes(self.user_id))
        data = {
            'user': self.user,
            'domain': Site.objects.first().domain,
            'token': token,
            'uid': uid,
        }
        message = render_to_string('member/mails/email_validation_email.txt', data)
        html = render_to_string('member/mails/email_validation_email.html', data)
        self.user.email_user(subject, message, html_message=html)

    class Meta:
        verbose_name = _("profile")
        verbose_name_plural = _("profiles")

    def get_absolute_url(self):
        return reverse_lazy("member:user_detail", args=(self.user.pk,))


class Person(Profile):
    telephone = models.CharField(
        blank=True,
        max_length=32,
        verbose_name=_("telephone"),
    )
    pgp = models.CharField(
        blank=True,
        default="",
        max_length=40,
        verbose_name=_("PGP fingerprint"),
    )

    @property
    def current_membership(self):
        curr = datetime.now().date()
        for mem in self.memberships.all():
            if mem.date_start <= curr <= mem.date_end:
                return mem
        return None

    @property
    def is_person(self):
        return True

    @property
    def has_current_membership(self):
        return self.current_membership is not None

    def __str__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = _("person")
        verbose_name_plural = _("persons")

    def update_membership(self):
        for membership in self.memberships.all():
            membership.delete()
        # get validity dates
        purchases = [(
            pur.purchase_entry.invoice.date,
            pur.purchase_entry.number,
            pur.purchase_entry.product.duration,
        ) for pur in self.membership_purchase.all()]
        if len(purchases) == 0:
            return
        purchases.sort()  # by increasing date
        date_start = purchases[0][0]
        date_end = date_start
        for (date, num, dur) in purchases:
            if date_end < date:
                # create new period of membership
                membership = Membership(member=self, date_start=date_start, date_end=date_end)
                membership.save()
                # prepare for next period
                date_start = date_end = date
            date_end += num * dur
        # create latest period of membership
        membership = Membership(member=self, date_start=date_start, date_end=date_end)
        membership.save()


class Organization(Profile):
    manager = models.ForeignKey(
        'member.Person',
        on_delete=models.PROTECT,
        related_name='organizations',
        verbose_name=_("manager"),
    )
    members = models.ManyToManyField(
        'member.Person',
        verbose_name=_("members"),
    )

    @property
    def is_person(self):
        return False

    @property
    def has_current_membership(self):
        return self.manager.has_current_membership

    @property
    def current_membership(self):
        return self.manager.current_membership

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = _("organization")
        verbose_name_plural = _("organizations")


class Membership(models.Model):
    member = models.ForeignKey(
        'member.Person',
        on_delete=models.PROTECT,
        related_name='memberships',
        verbose_name=_("member"),
    )
    date_start = models.DateField(
        verbose_name=_("start membership date"),
    )
    date_end = models.DateField(
        null=True,
        verbose_name=_("end membership date"),
    )

    def __str__(self):
        return _("from {start} to {end}").capitalize().format(
            member=self.member,
            start=self.date_start,
            end=self.date_end,
        )

    class Meta:
        verbose_name = _("membership")
        verbose_name_plural = _("memberships")
