from django.contrib.auth.models import User
from django.core.management import BaseCommand
from django.db.models import Q, Count


class Command(BaseCommand):
    def handle(self, *args, **options):
        """
        Since we added custom validators to check that emails are unique
        and that usernames are well-formed, we need to perform the
        validation manually in case of direct update in the database.
        """
        error = False

        # Check that email addresses are valid.
        for user in User.objects.filter(Q(email="") | Q(email__isnull=True)).all():
            error = True
            if options['verbosity'] >= 1:
                self.stderr.write(self.style.ERROR(f"User {user.username} has no email address!"))

        # Check that email addresses are unique.
        for item in User.objects.annotate(email_count=Count('email')).values('email', 'email_count'):
            email, count = item['email'], item['email_count']
            if email and count > 1:
                error = True
                if options['verbosity'] >= 1:
                    self.stderr.write(self.style.ERROR(f"Email address {email} is used {count} times!"))

        # Check that all usernames are valid.
        for user in User.objects.filter(~Q(username__regex="^[a-z][a-z0-9-]+$")).all():
            error = True
            if options['verbosity'] >= 1:
                self.stderr.write(self.style.ERROR(f"Username {user.username} is not a valid username!"))

        if error:
            if options['verbosity'] >= 2:
                self.stderr.write(self.style.ERROR("There are integrity errors in your database. "
                                                   "Please fix them."))
            exit(1)
