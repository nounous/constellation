from django.contrib.auth.models import User
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.filters import OrderingFilter

from .serializers import MembershipSerializer, UserSerializer
from ..models import Membership


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('username', 'last_name', 'first_name', 'email', 'profile__address',
                        'profile__member__telephone', 'profile__member__pgp',
                        'profile__organization__manager__user__username',
                        'profile__organization__manager__user__last_name',
                        'profile__organization__manager__user__first_name',
                        'profile__organization__manager__user__email',
                        'profile__organization__manager__address',
                        'profile__organization__manager__telephone',
                        'profile__polymorphic_ctype',)
    lookup_field = 'username'


class MembershipViewSet(viewsets.ModelViewSet):
    queryset = Membership.objects.all()
    serializer_class = MembershipSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter,)
    filterset_fields = ('date_start', 'date_end', 'member__user__username', 'member__user__last_name',
                        'member__user__first_name', 'member__user__email',)
    ordering_fields = ('date_start', 'date_end',)
