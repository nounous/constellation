from rest_framework.routers import BaseRouter

from . import views


def register_routes(router: BaseRouter) -> None:
    router.register('auth/user', views.UserViewSet)
    router.register('member/membership', views.MembershipViewSet)
