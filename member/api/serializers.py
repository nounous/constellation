from django.contrib.auth.models import User
from rest_framework import serializers

from constellation.serializers import PolymorphicSerializer
from ..models import Person, Membership, Organization


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = '__all__'


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = '__all__'


class ProfileSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        Person: PersonSerializer,
        Organization: OrganizationSerializer,
    }


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ('id', 'is_superuser', 'username', 'first_name', 'email',
                  'is_staff', 'groups', 'user_permissions', 'profile',)


class MembershipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Membership
        fields = '__all__'
