from collections import namedtuple

from django import forms
from django.contrib.admin.widgets import AutocompleteSelectMultiple
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _

from member.models import Person, Organization


class SignupForm(UserCreationForm):
    gtu = forms.BooleanField(
        label=_("I read and accept the general terms of use."),
        required=True,
    )

    no_double_account = forms.BooleanField(
        label=_("I certify that I don't have already an account."),
        required=True,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['last_name'].required = True
        self.fields['email'].required = True
        self.fields['username'].help_text = _("Required. 32 characters or fewer. Lowercase letters, "
                                              "digits and dashes only.")

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'password1', 'password2', 'gtu', 'no_double_account',)


class UserForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['last_name'].required = True
        self.fields['email'].required = True
        self.fields['username'].help_text = _("Required. 32 characters or fewer. Lowercase letters, "
                                              "digits and dashes only.")

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email',)


class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ('address', 'telephone',)


class OrganizationForm(forms.ModelForm):

    class Meta:
        model = Organization
        fields = ('address', 'members',)
        widgets = {
            'members': AutocompleteSelectMultiple(
                field=Organization._meta.get_field('members'),
                admin_site=namedtuple('AdminSite', 'name')('member'),  # Use local autocomplete view
            ),
        }
