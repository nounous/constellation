from django.contrib.auth.models import User


class ImpersonateMiddleware:
    """
    Define the current user in case of impersonation.
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if "_fake_user_id" in request.session:
            request.user = User.objects.get(pk=request.session["_fake_user_id"])
        return self.get_response(request)
