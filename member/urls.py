from django.contrib import admin
from django.contrib.admin.views.autocomplete import AutocompleteJsonView
from django.urls import path

from . import views

app_name = 'member'
urlpatterns = [
    path("signup/", views.SignupView.as_view(), name='signup'),
    path("detail/<int:pk>/", views.UserDetailView.as_view(), name='user_detail'),
    path("update/<int:pk>/", views.UserUpdateView.as_view(), name='user_update'),
    path('validate_email/resend/<int:pk>/', views.UserResendValidationEmailView.as_view(),
         name='email_validation_resend'),
    path('validate_email/<uidb64>/<token>/', views.UserValidateView.as_view(), name='email_validation'),
    path("impersonate/<int:pk>/", views.UserImpersonateView.as_view(), name="user_impersonate"),
    path("reset-impersonate/", views.ResetAdminView.as_view(), name="reset_impersonate"),
    # Autocomplete view that don't require to be staff
    path("autocomplete/", AutocompleteJsonView.as_view(admin_site=admin.site), name="autocomplete"),
]
