from datetime import timedelta

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from .models import Person, Membership, Organization
from .tokens import email_validation_token


class TestMembers(TestCase):
    def setUp(self):
        """
        Setup an admin user, a simple member and a simple organization.
        """
        self.admin = User.objects.create_superuser(
            username="admin",
            email="admin@example.com",
            password="adminadmin",
        )
        self.client.force_login(self.admin)

        self.member = User.objects.create(
            first_name="Member",
            last_name="MEMBER",
            username="member",
            email="member@example.com",
            password="member",
        )
        Person.objects.create(
            user=self.member,
            address="France",
            telephone="0123456789",
        )

        self.organization = User.objects.create(
            first_name="Organization",
            last_name="ORGANIZATION",
            username="organization",
            email="organization@example.com",
            password="organization",
        )
        Organization.objects.create(
            user=self.organization,
            address="France",
            manager=self.member.profile,
        )

        self.membership = Membership.objects.create(
            member=self.admin.profile,
            date_start=timezone.now(),
            date_end=timezone.now() + timedelta(days=366),
        )

    def test_admin(self):
        """
        Load admin pages and check that they are correctly rendered.
        """
        response = self.client.get(reverse('admin:auth_user_changelist'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:auth_user_change', args=(self.admin.pk,)))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:auth_user_change', args=(self.member.pk,)))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:auth_user_change', args=(self.organization.pk,)))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('admin:member_profile_changelist'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:member_profile_change', args=(self.admin.pk,)))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:member_profile_change', args=(self.member.pk,)))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:member_profile_change', args=(self.organization.pk,)))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('admin:member_person_changelist'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:member_person_change', args=(self.admin.pk,)))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:member_person_change', args=(self.member.pk,)))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('admin:member_organization_changelist'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:member_organization_change', args=(self.organization.pk,)))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('admin:member_membership_changelist'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:member_membership_change', args=(self.membership.pk,)))
        self.assertEqual(response.status_code, 200)
        # Filter by (in)active memberships
        response = self.client.get(reverse('admin:member_membership_changelist') + "?active_memberships=1")
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:member_membership_changelist') + "?active_memberships=0")
        self.assertEqual(response.status_code, 200)

    def test_signup(self):
        """
        Try to register a new user and check for some errors.
        """
        response = self.client.get(reverse('member:signup'))
        self.assertEqual(response.status_code, 200)

        # Normal signup
        response = self.client.post(reverse('member:signup'), data={
            'first_name': "Toto",
            'last_name': "TOTO",
            'username': "toto",
            'email': "toto@example.com",
            'password1': "toto1234",
            'password2': "toto1234",
            'address': "France",
            'telephone': "0100000000",
            'gtu': True,
            'no_double_account': True,
        })
        self.assertTrue(User.objects.filter(username="toto"))
        user = User.objects.get(username="toto")
        self.assertRedirects(response, reverse('member:user_detail', args=(user.pk,)))

        # Email is already taken
        response = self.client.post(reverse('member:signup'), data={
            'first_name': "Toto",
            'last_name': "TOTO",
            'username': "toto2",
            'email': "toto@example.com",
            'password1': "toto1234",
            'password2': "toto1234",
            'address': "France",
            'telephone': "0100000000",
            'gtu': True,
            'no_double_account': True,
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('email', response.context['form'].errors)

        # Username is already taken
        response = self.client.post(reverse('member:signup'), data={
            'first_name': "Toto",
            'last_name': "TOTO",
            'username': "toto",
            'email': "toto2@example.com",
            'password1': "toto1234",
            'password2': "toto1234",
            'address': "France",
            'telephone': "0100000000",
            'gtu': True,
            'no_double_account': True,
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('username', response.context['form'].errors)

        # Member form is invalid
        response = self.client.post(reverse('member:signup'), data={
            'first_name': "Toto",
            'last_name': "TOTO",
            'username': "toto2",
            'email': "toto2@example.com",
            'password1': "pouetpouet",
            'password2': "pouetpouet",
            'address': "",
            'telephone': "",
            'gtu': True,
            'no_double_account': True,
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('address', response.context['member_form'].errors)

    def test_email_validation(self):
        """
        Test to validate email address.
        """
        # Send a validation link
        response = self.client.get(reverse("member:email_validation_resend", args=(self.admin.pk,)))
        self.assertRedirects(response, self.admin.profile.get_absolute_url())

        # Check that the email validation link is valid
        token = email_validation_token.make_token(self.admin)
        uid = urlsafe_base64_encode(force_bytes(self.admin.pk))
        response = self.client.get(reverse("member:email_validation", kwargs=dict(uidb64=uid, token=token)))
        self.assertEqual(response.status_code, 200)
        self.admin.profile.refresh_from_db()
        self.assertTrue(self.admin.profile.email_confirmed)

        # Token has expired
        response = self.client.get(reverse("member:email_validation", kwargs=dict(uidb64=uid, token=token)))
        self.assertEqual(response.status_code, 400)

        # Uid does not exist
        response = self.client.get(reverse("member:email_validation", kwargs=dict(uidb64=0, token="toto")))
        self.assertEqual(response.status_code, 400)

    def test_user_detail(self):
        """
        Display the page of the detail of a user.
        """
        response = self.client.get(reverse('member:user_detail', args=(self.admin.pk,)))
        self.assertEqual(response.status_code, 200)

    def test_user_update(self):
        """
        Try to update user information.
        """
        response = self.client.get(reverse('member:user_update', args=(self.admin.pk,)))
        self.assertEqual(response.status_code, 200)

        # Valid update
        response = self.client.post(reverse('member:user_update', args=(self.admin.pk,)), data={
            'first_name': "Admin",
            'last_name': "ADMIN",
            'username': "admin",
            'email': "god@example.com",
            'address': "Earth",
            'telephone': "0123456789",
        })
        self.assertRedirects(response, reverse('member:user_detail', args=(self.admin.pk,)))
        self.admin.refresh_from_db()
        self.assertEqual(self.admin.email, "god@example.com")

        # Email is already taken
        response = self.client.post(reverse('member:user_update', args=(self.admin.pk,)), data={
            'first_name': "Admin",
            'last_name': "ADMIN",
            'username': "admin",
            'email': "member@example.com",
            'address': "Earth",
            'telephone': "0123456789",
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('email', response.context['form'].errors)

        # Member form is invalid
        response = self.client.post(reverse('member:user_update', args=(self.admin.pk,)), data={
            'first_name': "Admin",
            'last_name': "ADMIN",
            'username': "admin",
            'email': "admin@example.com",
            'address': "",
            'telephone': "",
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('address', response.context['profile_form'].errors)

        # Invalid username
        response = self.client.post(reverse('member:user_update', args=(self.admin.pk,)), data={
            'first_name': "Admin",
            'last_name': "ADMIN",
            'username': "#admin#",
            'email': "god@example.com",
            'address': "Earth",
            'telephone': "0123456789",
        })
        self.assertEqual(response.status_code, 200)
        self.assertIn('username', response.context['form'].errors)

    def test_impersonate(self):
        """
        Admin can impersonate other people to act as them.
        """
        response = self.client.get(reverse("member:user_impersonate", args=(0x7ffff42ff,)))
        self.assertEqual(response.status_code, 404)

        # Impersonate other account
        user = User.objects.create(username="toto", email="toto@example.com")
        response = self.client.get(reverse("member:user_impersonate", args=(user.pk,)))
        self.assertRedirects(response, reverse("member:user_detail", args=(user.pk,)), 302, 200)
        self.assertEqual(self.client.session["_fake_user_id"], user.id)

        # Reset admin view
        response = self.client.get(reverse("member:reset_impersonate"))
        self.assertRedirects(response, reverse("index"), 302, 200)
        self.assertFalse("_fake_user_id" in self.client.session)
