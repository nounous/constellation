from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from .models import Person, Profile


def create_superuser_profile(instance, **_):
    """
    When creating a superuser from the command line,
    it might not have an associated profile.
    We create it automatically to ensure that the database is consistent.
    """
    if instance.is_superuser and not Profile.objects.filter(user=instance).exists():
        Person.objects.create(user=instance)


def ensure_email_not_empty(instance, **_ignored):
    """
    Django authorizes users to don't have any email address.
    We don't permit that and check that is not empty.
    """
    if not instance.email:
        raise ValidationError(_("Email address is required."))


def send_email_address_confirmation(instance, created, **_ignored):
    """
    When an account is created, send a mail with a validation link.
    """
    if created:
        instance.send_email_validation_link()
