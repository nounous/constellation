import importlib

from django.conf import settings
from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.text import capfirst
from django.utils.translation import gettext_lazy as _
from polymorphic.admin import PolymorphicChildModelAdmin, PolymorphicInlineSupportMixin, PolymorphicParentModelAdmin, \
    StackedPolymorphicInline

from .forms import SignupForm
from .models import Person, Membership, Organization, Profile


class ProfileInline(StackedPolymorphicInline):
    """
    Manage the associated of a target user.
    This should be used as an inline for the User ModelAdmin.
    To register it, put `member.admin.ProfileInline` in the list
    `ADMIN_USER_INLINES` in the settings.

    This inline is polymorphic and can be as well a person or
    an organization. More details here:
    https://django-polymorphic.readthedocs.io/en/stable/admin.html#inline-models
    """
    class PersonInline(StackedPolymorphicInline.Child):
        """
        Profile inline for persons
        """
        model = Person

    class OrganizationInline(StackedPolymorphicInline.Child):
        """
        Profile inline for organizations
        """
        model = Organization
        autocomplete_fields = ('manager', 'members',)

    model = Profile
    child_inlines = (PersonInline, OrganizationInline, )

    def get_view_on_site_url(self, obj=None):
        # Redirect to the user detail page with a custom front page
        return reverse_lazy('admin:member_profile_change', args=(obj.pk,)) if obj else None


# Unregister default view for users in order to use custom page view
admin.site.unregister(User)


@admin.register(User)
class CustomUserAdmin(PolymorphicInlineSupportMixin, UserAdmin):
    """
    Manage registered users.
    This extends the default UserAdmin view to add extra data.

    The user list can be filtered by members and organizers.

    Applications can register their own inline for this view,
    by registering the inline in the `ADMIN_USER_INLINES`
    setting, like `member.admin.ProfileInline` for example.
    """

    class ProfileTypeFilter(SimpleListFilter):
        """
        Filter the user list by its profile type,
        ie. member or organization.
        """
        title = _("profile type")
        parameter_name = 'profile_type'

        def lookups(self, request, model_admin):
            # Get all Profile subclasses
            return [(ctype.id, ctype.model_class()._meta.verbose_name.capitalize())
                    for ctype in ContentType.objects.all()
                    if ctype.model_class() and issubclass(ctype.model_class(), Profile)
                    and ctype.model_class() != Profile]

        def queryset(self, request, queryset):
            # Filter queryset by profile type if necessary
            return queryset.filter(profile__polymorphic_ctype_id=self.value()) if self.value() else queryset

    # Add profile type in table and filters
    list_display = ('username', 'email', 'first_name', 'last_name', 'profile_type', 'is_superuser',)
    list_filter = (ProfileTypeFilter, 'is_superuser', 'groups',)
    add_form = SignupForm
    add_fieldsets = (
        (_("Personal info"), {
            'classes': ('wide',),
            'fields': ('first_name', 'last_name', 'username', 'email',
                       'password1', 'password2',),
        },),
        (_("General Terms of Use"), {
            'classes': ('wide',),
            'fields': ('gtu', 'no_double_account',),
        })
    )

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_("Personal info"), {'fields': ('first_name', 'last_name', 'email', 'impersonate_link')}),
        (_("Permissions"), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_("Important dates"), {'fields': ('last_login', 'date_joined')}),
    )
    readonly_fields = ('impersonate_link',)

    def profile_type(self, obj):
        # Display profile type name
        return obj.profile._meta.verbose_name.capitalize()
    profile_type.short_description = _("profile type")

    def impersonate_link(self, obj):
        text = capfirst(_("impersonate"))
        return mark_safe("<a class=\"submit-row\" "
                         f"href=\"{reverse_lazy('member:user_impersonate', args=(obj.pk,))}\">{text}</a>")
    impersonate_link.short_description = _("impersonate")

    def get_view_on_site_url(self, obj=None):
        # Redirect to the user detail page from the main front
        return reverse_lazy('member:user_detail', args=(obj.pk,)) if obj else None

    def get_inlines(self, request, obj):
        # Query custom inlines from settings
        inlines = []
        for inline_name in settings.ADMIN_USER_INLINES:
            split = inline_name.rsplit('.', 1)
            module = importlib.import_module(split[0])
            inline = getattr(module, split[1])
            inlines.append(inline)
        return inlines


@admin.register(Profile)
class ProfileAdmin(PolymorphicParentModelAdmin):
    """
    Manage profile data, according to its type, member
    or organization. This is a polymorphic ModelAdmin.
    """
    # Declare child models
    child_models = [Person, Organization]
    list_display = ('username', 'email', 'first_name', 'last_name',)
    search_fields = ('user__username', 'user__email', 'user__first_name', 'user__last_name',)
    readonly_fields = ('user',)

    def username(self, obj):
        return obj.user.username
    username.short_description = _("username")

    def email(self, obj):
        return obj.user.email
    email.short_description = _("email")

    def first_name(self, obj):
        return obj.user.first_name
    first_name.short_description = _("first name")

    def last_name(self, obj):
        return obj.user.last_name
    last_name.short_description = _("last name")

    def has_add_permission(self, request):
        # Don't add manually a profile
        return False

    def has_delete_permission(self, request, obj=None):
        # Don't delete a profile
        # Please update information through the user detail page
        return False


class AbstractProfileAdmin(PolymorphicChildModelAdmin):
    # This class is used to factorize code for subclasses
    def username(self, obj):
        return obj.user.username
    username.short_description = _("username")

    def email(self, obj):
        return obj.user.email
    email.short_description = _("email")

    def first_name(self, obj):
        return obj.user.first_name
    first_name.short_description = _("first name")

    def last_name(self, obj):
        return obj.user.last_name
    last_name.short_description = _("last name")

    def has_add_permission(self, request):
        # Don't add manually a profile
        return False

    def has_delete_permission(self, request, obj=None):
        # Don't delete a profile
        # Please update information through the user detail page
        return False


class MembershipInline(admin.StackedInline):
    """
    Manage memberships of a target member.
    This should be used as an inline for the Person ModelAdmin.
    """
    model = Membership
    extra = 0

    def get_view_on_site_url(self, obj=None):
        # Redirect to the membership detail view
        return reverse_lazy('admin:member_membership_change', args=(obj.pk,)) if obj else None


@admin.register(Person)
class PersonAdmin(AbstractProfileAdmin):
    """
    Manage member data
    """
    search_fields = ('user__username', 'user__first_name', 'user__last_name', 'user__email',)
    list_display = ('username', 'email', 'first_name', 'last_name',)
    autocomplete_fields = ('user',)
    inlines = (MembershipInline, )

    def has_view_permission(self, request, obj=None):
        # Organization managers can autocomplete their members
        return super().has_view_permission(request, obj) or request.user.profile.is_organization


@admin.register(Organization)
class OrganizationAdmin(AbstractProfileAdmin):
    """
    Manage organization data
    """
    search_fields = ('user__username', 'user__first_name', 'user__last_name', 'user__email', 'manager',)
    list_display = ('username', 'email', 'first_name', 'last_name', 'manager',)
    autocomplete_fields = ('user', 'manager', 'members',)


@admin.register(Membership)
class MembershipAdmin(admin.ModelAdmin):
    """
    Manage memberships.

    Memberships can be sorted and filtered by date and validity status.
    """
    class ActiveMembershipFilter(SimpleListFilter):
        """
        Filter memberships by valid (current) memberships
        """
        title = _("active memberships")
        parameter_name = 'active_memberships'

        def lookups(self, request, model_admin):
            return [
                ('1', _('Yes')),
                ('0', _('No')),
            ]

        def queryset(self, request, queryset):
            # Filter memberships
            if self.value() is not None:
                # Get current memberships
                membership_filter = Q(date_start__lte=timezone.now()) \
                    & (Q(date_end__gte=timezone.now()) | Q(date_end__isnull=True))
                if self.value() == '0':
                    # Get past (or future) memberships
                    membership_filter = ~membership_filter
                queryset = queryset.filter(membership_filter)
            return queryset

    date_hierarchy = 'date_start'
    list_display = ('member', 'date_start', 'date_end',)
    list_filter = (ActiveMembershipFilter, 'date_start', 'date_end',)
    search_fields = ('member__user__username', 'member__user__last_name', 'member__user__first_name',
                     'member__user__email', 'date_start', 'date_end',)
    autocomplete_fields = ('member',)
