#!/bin/sh

# Set up Django project
python3 manage.py collectstatic --noinput
python3 manage.py compilemessages
python3 manage.py compilejsmessages
python3 manage.py migrate

if [ "$1" ]; then
    # Command passed
    echo "Running $@..."
    $@
else
    # Launch server
    if [ "$DJANGO_APP_STAGE" = "prod" ]; then
        uwsgi --http-socket 0.0.0.0:8080 --master --plugins python3 \
              --module constellation.wsgi:application --env DJANGO_SETTINGS_MODULE=constellation.settings \
              --processes 4 --static-map /static=/var/local/constellation/static --harakiri=20 --max-requests=5000 --vacuum
    else
        python3 manage.py runserver 0.0.0.0:8080;
    fi
fi
