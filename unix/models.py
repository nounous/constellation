from django.db import models
from django.utils.translation import gettext_lazy as _


class Passwd(models.Model):
    uid = models.CharField(
        max_length=32,
        verbose_name=_("user ID"),
    )
    password = models.TextField(
        verbose_name=_("password"),
    )
    uid_number = models.PositiveIntegerField(
        verbose_name=_("user ID number"),
    )
    gid_number = models.PositiveIntegerField(
        verbose_name=_("group ID number"),
    )
    gecos = models.TextField(
        verbose_name=_("GECOS"),
    )
    home = models.TextField(
        verbose_name=_("home directory"),
    )
    shell = models.TextField(
        verbose_name=_("shell"),
    )

    def __str__(self):
        return self.uid

    class Meta:
        verbose_name = _("UNIX user")
        verbose_name_plural = _("UNIX users")


class Group(models.Model):
    gid = models.CharField(
        max_length=32,
        verbose_name=_("group ID"),
    )
    password = models.TextField(
        blank=True,
        default="",
        verbose_name=_("password"),
    )
    gid_number = models.PositiveIntegerField(
        verbose_name=_("group ID number"),
    )
    member_uids = models.ManyToManyField(
        Passwd,
        related_name='groups',
        verbose_name=_("members"),
    )

    def __str__(self):
        return self.gid

    class Meta:
        verbose_name = _("UNIX group")
        verbose_name_plural = _("UNIX groups")


class Subuid(models.Model):
    uid = models.ForeignKey(
        Passwd,
        on_delete=models.CASCADE,
        related_name='subuids',
        verbose_name=_("UNIX user"),
    )
    subuid = models.PositiveIntegerField(
        verbose_name=_("sub-user ID"),
    )
    subuid_count = models.PositiveIntegerField(
        verbose_name=_("sub-user ID count"),
    )

    def __str__(self):
        return _("Subuid {subuid} for {uid}").format(subuid=self.subuid, uid=self.uid)

    class Meta:
        verbose_name = _("UNIX sub-user")
        verbose_name_plural = _("UNIX sub-users")


class Subgid(models.Model):
    uid = models.ForeignKey(
        Passwd,
        on_delete=models.CASCADE,
        related_name='subgids',
        verbose_name=_("UNIX user"),
    )
    subgid = models.PositiveIntegerField(
        verbose_name=_("sub-group ID"),
    )
    subgid_count = models.PositiveIntegerField(
        verbose_name=_("sub-group ID count"),
    )

    def __str__(self):
        return _("Subgid {subgid} for {uid}").format(subgid=self.subgid, uid=self.uid)

    class Meta:
        verbose_name = _("UNIX sub-group")
        verbose_name_plural = _("UNIX sub-groups")
