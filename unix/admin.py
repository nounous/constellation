from django.contrib import admin

from .models import Passwd, Group


@admin.register(Passwd)
class PasswdAdmin(admin.ModelAdmin):
    list_display = ('uid', 'uid_number', 'gid_number', 'gecos')


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ('gid', 'gid_number')
