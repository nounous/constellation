from django.db import models
from django.utils.translation import gettext_lazy as _
from polymorphic.models import PolymorphicModel

from layers.models import MACAddressField


class FireWallActionField(models.CharField):

    CHOICES = [
        ('ACCEPT', 'Accept'),
        ('DROP', 'Drop'),
        ('REJECT', 'Reject')
    ]

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 16
        kwargs['choices'] = FireWallActionField.CHOICES
        super().__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        del kwargs['max_length']
        del kwargs['choices']
        return name, path, args, kwargs


class FileWallLayer(PolymorphicModel):
    name = models.SlugField(
        verbose_name=_("name"),
    )
    out = models.BooleanField(
        null=True,
        verbose_name=_("output"),
    )
    action = FireWallActionField(
        verbose_name=_("action"),
    )

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class FireWallLayer2(FileWallLayer):
    mac_address = MACAddressField(
        verbose_name=_("MAC address")
    )

    class Meta:
        verbose_name = _("layer 2 firewall")
        verbose_name_plural = _("layer 2 firewalls")


class FireWallLayer3(FileWallLayer):
    prefix = models.GenericIPAddressField(
        protocol='both',
        verbose_name=_("prefix"),
    )
    length = models.PositiveSmallIntegerField(
        verbose_name=_("length"),
    )

    class Meta:
        verbose_name = _("layer 3 firewall")
        verbose_name_plural = _("layer 3 firewalls")


class Layer4Protocol(models.Model):

    IP_PROTOCOLS = [
        (1, 'ICMP'),
        (6, 'TCP'),
        (17, 'UDP'),
    ]

    protocol = models.PositiveSmallIntegerField(
        choices=IP_PROTOCOLS,
        unique=True,
        verbose_name=_("protocol"),
    )
    name = models.SlugField(
        verbose_name=_("name"),
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("layer 4 protocol")
        verbose_name_plural = _("layer 4 protocols")


class FireWallLayer4(FileWallLayer):
    protocol = models.ForeignKey(
        Layer4Protocol,
        on_delete=models.PROTECT,
        related_name='+',
        verbose_name=_("protocol"),
    )
    port_start = models.PositiveIntegerField(
        null=True,
        verbose_name=_("port start"),
    )
    port_end = models.PositiveIntegerField(
        null=True,
        verbose_name=_("port end"),
    )

    class Meta:
        verbose_name = _("layer 4 firewall")
        verbose_name_plural = _("layer 4 firewalls")


class FireWallRule(models.Model):
    name = models.SlugField()
    layer2 = models.ForeignKey(
        FireWallLayer2,
        null=True,
        on_delete=models.PROTECT,
        related_name='+',
        verbose_name=_("layer 2 firewall"),
    )
    layer3 = models.ForeignKey(
        FireWallLayer3,
        null=True,
        on_delete=models.PROTECT,
        related_name='+',
        verbose_name=_("layer 3 firewall"),

    )
    layer4 = models.ForeignKey(
        FireWallLayer4,
        null=True,
        on_delete=models.PROTECT,
        related_name='+',
        verbose_name=_("layer 4 firewall"),
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("firewall rule")
        verbose_name_plural = _("firewall rules")


class FireWall(models.Model):
    interface = models.OneToOneField(
        'layers.Interface',
        on_delete=models.CASCADE,
        related_name='firewall',
        verbose_name=_("interface"),
    )
    rules = models.ManyToManyField(
        FireWallRule,
        verbose_name=_("rules"),
    )

    def __str__(self):
        return _("firewall for {interface}").format(interface=self.interface)

    class Meta:
        verbose_name = _("firewall")
        verbose_name_plural = _("firewalls")
