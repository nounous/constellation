from django import forms
from django.contrib import admin

from django.db import models
from django.urls import reverse_lazy
from django.utils.safestring import mark_safe
from django.utils.text import capfirst
from django.utils.translation import gettext_lazy as _
from django.utils.dateparse import parse_duration
from polymorphic.admin import PolymorphicChildModelAdmin, PolymorphicParentModelAdmin

from .models import ComnpayPaymentMethod, Entry, Invoice, \
    MembershipProduct, MembershipPurchase, PaymentMethod, Product
from .templatetags.pretty_money import pretty_money

import datetime


class AmountInput(forms.NumberInput):
    """
    This input type lets the user type amounts in euros, but forms receive data in cents.
    """
    template_name = "billing/admin/amount_input.html"

    def format_value(self, value):
        return None if value is None or value == "" else f"{value / 100:.02f}"

    def value_from_datadict(self, data, files, name):
        val = super().value_from_datadict(data, files, name)
        return str(int(100 * float(val))) if val else val


class DurationInput(forms.NumberInput):
    """
    This input type lets the user type the number of days and converts it to a timedelta
    """
    template_name = "billing/admin/duration_input.html"

    def format_value(self, duration):
        return f"{parse_duration(duration).days}" if duration else duration

    def value_from_datadict(self, data, files, name):
        val = super().value_from_datadict(data, files, name)
        return str(datetime.timedelta(days=int(val))) if val else val


@admin.register(Product)
class ProductAdmin(PolymorphicParentModelAdmin):
    """
    Manage product types.
    """
    child_models = [Product, MembershipProduct]
    list_display = ('name', 'pretty_price', 'payer_type',)
    list_filter = ('payer_type',)
    search_fields = ('name',)
    # Replace the amount widget with a widget that is more intuitive for the user
    formfield_overrides = {
        models.IntegerField: {'widget': AmountInput},
    }

    def pretty_price(self, obj):
        # Format price to a human-readable value
        return pretty_money(obj.price)
    pretty_price.short_description = _("price")


@admin.register(MembershipProduct)
class MembershipProductAdmin(PolymorphicChildModelAdmin):
    list_display = ('name', 'pretty_price', 'payer_type', 'duration_days',)

    formfield_overrides = {
        models.IntegerField: {'widget': AmountInput},
        models.DurationField: {'widget': DurationInput},
    }

    def pretty_price(self, obj):
        return pretty_money(obj.price)
    pretty_price.short_description = _("price")

    def duration_days(self, obj):
        return "{} days".format(obj.duration.days)
    duration_days.short_description = _("duration")


@admin.register(MembershipPurchase)
class MembershipPurchaseAdmin(admin.ModelAdmin):
    list_display = ('member', 'purchase_entry',)
    search_fields = ('member__username', 'member__email', 'member__last_name', 'member__first_name',)

    def has_add_permission(self, request):
        # Read-only
        return False

    def has_change_permission(self, request, obj=None):
        # Read only
        return False

    def has_delete_permission(self, request, obj=None):
        # Read only
        return False


@admin.register(Entry)
class EntryAdmin(admin.ModelAdmin):
    """
    Manage purchases that are related to an invoice.
    These entries should not be added or edited manually,
    even if this view is not in read only mode.
    This is only useful in debug cases.

    Please note that you can't update an entry that is linked
    to a valid invoice. It must be invalidated first if necessary.
    """
    list_display = ('invoice', 'product', 'number', 'pretty_price',)
    list_filter = ('product__name',)
    search_fields = ('product__name',)
    autocomplete_fields = ('invoice', 'product',)
    readonly_fields = ('pretty_price', 'invoice',)
    # Don't display cents, but human-readable amount
    exclude = ('price',)

    def pretty_price(self, obj):
        # Format price to a human-readable value
        return pretty_money(obj.price)
    pretty_price.short_description = _("price")

    def has_add_permission(self, request):
        # Don't add manually an entry to an invoice
        return False

    def has_change_permission(self, request, obj=None):
        # Don't update an entry of a valid invoice
        return False if obj and obj.invoice.valid else super().has_change_permission(request, obj)

    def has_delete_permission(self, request, obj=None):
        # Don't delete an entry of a valid invoice
        return False if obj and obj.invoice.valid else super().has_delete_permission(request, obj)


class EntryInline(admin.StackedInline):
    """
    Manage purchases of a target invoice.
    This should be used as an inline for the Invoice ModelAdmin.

    Entries can't be updated once the associated invoice got validated.
    """
    model = Entry
    extra = 0
    autocomplete_fields = ('product',)
    readonly_fields = ('pretty_price',)
    # Don't display cents, but human-readable amount
    exclude = ('price',)

    def pretty_price(self, obj):
        # Format price to a human-readable value
        return pretty_money(obj.price) if obj.price else None
    pretty_price.short_description = _("price")

    def get_view_on_site_url(self, obj=None):
        # Detail link of an entry is the Django-admin update page
        return reverse_lazy('admin:billing_entry_change', args=(obj.pk,)) if obj else None

    def has_add_permission(self, request, obj):
        # Don't add any entry to a valid or a validated invoice
        return False if obj and (obj.valid or Invoice.objects.get(pk=obj.pk).valid) \
            else super().has_add_permission(request, obj)

    def has_change_permission(self, request, obj=None):
        # Don't change any entry to a valid or a validated invoice
        return False if obj and (obj.valid or Invoice.objects.get(pk=obj.pk).valid) \
            else super().has_change_permission(request, obj)

    def has_delete_permission(self, request, obj=None):
        # Don't delete any entry to a valid or a validated invoice
        return False if obj and (obj.valid or Invoice.objects.get(pk=obj.pk).valid) \
            else super().has_delete_permission(request, obj)


@admin.register(PaymentMethod)
class PaymentMethodAdmin(PolymorphicParentModelAdmin):
    """
    Manage different payment methods.
    """
    child_models = [PaymentMethod, ComnpayPaymentMethod]
    list_display = ('__str__', 'visible', 'payment_type',)
    list_filter = ('visible',)

    def payment_type(self, obj):
        return capfirst(obj.polymorphic_ctype.model_class()._meta.verbose_name)
    payment_type.short_description = _("payment type")


@admin.register(ComnpayPaymentMethod)
class ComnpayPaymentMethodAdmin(PolymorphicChildModelAdmin):
    """
    Manage Comnpay payment methods.

    In normal cases, there should be at most one object in this table.
    """
    list_display = ('__str__', 'visible',)
    list_filter = ('visible',)


@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    """
    Manage invoices.

    If the invoice is invalid, it is possible to update entries,
    validate the invoice or delete it. There is also a link that
    lets the superuser to finish the credit card payment if
    necessary.

    If the invoice is valid, then the invoice can't be modified
    neither deleted. It can only be invalidated or approved.
    The HTML invoice is generated and displayed.
    """
    list_display = ('__str__', 'target', 'payment_method', 'pretty_price', 'valid', 'approved',)
    list_filter = ('payment_method', 'valid', 'approved',)
    search_fields = ('id', 'target__username', 'target__first_name', 'target__last_name', 'target__email',
                     'products__name',)
    autocomplete_fields = ('target',)
    # Don't display template source
    exclude = ('html',)
    # Display invoice entries
    inlines = (EntryInline,)
    # Add custom actions to validate or approve a large range of invoices if necessary
    actions = ('mark_valid', 'mark_invalid', 'mark_approved', 'mark_disapproved',)

    def pretty_price(self, obj):
        # Format price to a human-readable value
        return pretty_money(obj.price)
    pretty_price.short_description = _("price")

    def rendered_invoice(self, obj):
        # Render invoice template
        return mark_safe(obj.html)
    rendered_invoice.short_description = _("rendered invoice")

    def rendered_invoice_link(self, obj):
        # Display link to the page of the invoice template
        text = capfirst(_("download"))
        return mark_safe(f"<a class=\"submit-row\" href=\"{obj.get_absolute_url()}\">{text}</a>")
    rendered_invoice_link.short_description = _("rendered invoice link")

    def pay_invoice(self, obj):
        # Display button to finish the credit-card payment
        text = capfirst(_('pay invoice'))
        return mark_safe(f"<a class=\"submit-row\" href=\"{obj.payment_method.get_payment_url(obj)}\">"
                         f"{text}</a>")
    pay_invoice.short_description = _("pay invoice")

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.pk and Invoice.objects.get(pk=obj.pk).valid:
            # A valid invoice can't be updated
            return 'target', 'payment_method', 'pretty_price', 'rendered_invoice', 'rendered_invoice_link',
        elif obj:
            # Display some read-only fields on non-validated invoices
            return 'target', 'pretty_price', 'pay_invoice', 'rendered_invoice_link',
        return ()

    def has_delete_permission(self, request, obj=None):
        # Don't delete a valid invoice
        if obj and obj.valid:
            return False
        return super().has_delete_permission(request, obj)

    def mark_valid(self, _request, queryset):
        # Update a large range of invoices to mark them as valid
        queryset.update(valid=True)
    mark_valid.short_description = _("Mark as valid")

    def mark_invalid(self, _request, queryset):
        # Update a large range of invoices to mark them as invalid
        queryset.update(valid=False)
    mark_invalid.short_description = _("Mark as invalid")

    def mark_approved(self, _request, queryset):
        # Update a large range of invoices to mark them as approved
        queryset.update(approved=True)
    mark_approved.short_description = _("Mark as approved")

    def mark_disapproved(self, _request, queryset):
        # Update a large range of invoices to mark them as not approved
        queryset.update(approved=False)
    mark_disapproved.short_description = _("Mark as disapproved")


class InvoiceInline(admin.StackedInline):
    """
    Manage invoices of a target user.
    This should be used as an inline for the User ModelAdmin.
    To register it, put `billing.admin.InvoiceInline` in the list
    `ADMIN_USER_INLINES` in the settings.

    Invoices are only displayed in the User ModelAdmin, and can
    only be validated and/or approved. Entries are not displayed
    on this page.
    """
    model = Invoice
    extra = 0
    # Don't display template source
    exclude = ('html',)
    readonly_fields = ('payment_method', 'pretty_price', 'rendered_invoice',)
    # Don't easily delete invoices
    can_delete = False

    def pretty_price(self, obj):
        # Format price to a human-readable value
        return pretty_money(obj.price)
    pretty_price.short_description = _("price")

    def rendered_invoice(self, obj):
        # Render invoice template
        return mark_safe(obj.html)
    rendered_invoice.short_description = _("rendered invoice")

    def get_view_on_site_url(self, obj=None):
        # Detail link of an invoice redirects to the detail page of the invoice in Django-Admin
        return reverse_lazy('admin:billing_invoice_change', args=(obj.pk,)) if obj else None
