from django.urls import path

from . import views


app_name = "billing"
urlpatterns = [
    path("purchase/<int:pk>/", views.PurchaseView.as_view(), name='purchase'),
    path("invoice_list/<int:pk>/", views.InvoiceListView.as_view(), name='invoice_list'),
    path('render-invoice/<int:pk>/', views.RenderInvoiceView.as_view(), name='render_invoice'),
    path('comnpay/redirect/<int:pk>/', views.RedirectComnpayView.as_view(), name='comnpay_redirect'),
    path('comnpay/ipn/', views.IPNComnpayView.as_view(), name='comnpay_ipn'),
    path('comnpay/success/', views.ComnpaySuccessView.as_view(), name='comnpay_success'),
    path('comnpay/fail/', views.ComnpayFailView.as_view(), name='comnpay_fail'),
]
