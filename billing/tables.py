from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
import django_tables2 as tables

from .models import Invoice


class InvoiceTable(tables.Table):
    display = tables.Column(
        verbose_name=_('display').capitalize,
        linkify=True,
        accessor="id",
        attrs={'a': {
            'class': 'btn btn-info',
        }},
    )

    def render_display(self):
        return mark_safe('<i class="bi bi-file-code"></i> ' + _("display").capitalize())

    class Meta:
        model = Invoice
        fields = ('id', 'date', 'products', 'valid', 'display',)
