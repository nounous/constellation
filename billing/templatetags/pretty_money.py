from django import template


register = template.Library()


@register.filter
def pretty_money(value):
    if value % 100 == 0:
        return f"{'- ' if value < 0 else ''}{abs(value) // 100} €"
    else:
        return f"{'- ' if value < 0 else ''}{abs(value) // 100}.{abs(value) % 100:02d} €"
