from django import template

from constellation import settings


register = template.Library()


@register.filter
def app_loaded(value):
    """
    Indicate if the given application is loaded.
    Let avoid import issues.
    """
    return value in settings.INSTALLED_APPS
