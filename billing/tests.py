import base64
import hashlib
from unittest import skipIf

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q
from django.test import TestCase
from django.urls import reverse

import datetime

from .models import ComnpayPaymentMethod, Entry, Invoice, MembershipProduct, PaymentMethod, Product


class TestBilling(TestCase):
    fixtures = ('initial',)

    def setUp(self):
        """
        Setup an admin user and some products.
        """
        self.admin = User.objects.create_superuser(
            username="admin",
            email="admin@example.com",
            password="adminadmin",
        )
        self.client.force_login(self.admin)

        self.payment_method = PaymentMethod.objects.create(
            name="test",
            visible=True,
        )

        self.product = Product.objects.create(
            name="random stuff",
            price=500,
            payer_type='all',
        )

        self.membership_product = MembershipProduct.objects.create(
            name="membership",
            price=1000,
            duration=datetime.timedelta(366),
            payer_type='per',
        )

        self.invoice = Invoice.objects.create(
            target=self.admin,
            payment_method=self.payment_method,
        )

        self.entry = Entry.objects.create(
            invoice=self.invoice,
            product=self.product,
            number=2,
        )

    def test_admin(self):
        """
        Load admin pages and check that they are correctly rendered.
        """
        response = self.client.get(reverse('admin:billing_invoice_changelist'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:billing_invoice_change', args=(self.invoice.pk,)))
        self.assertEqual(response.status_code, 200)
        # Validate invoice
        self.invoice.valid = True
        self.invoice.save()
        response = self.client.get(reverse('admin:billing_invoice_change', args=(self.invoice.pk,)))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('admin:billing_product_changelist'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:billing_product_change', args=(self.product.pk,)))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('admin:billing_membershipproduct_changelist'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:billing_membershipproduct_change',
                                           args=(self.membership_product.pk,)))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('admin:billing_entry_changelist'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:billing_entry_change', args=(self.entry.pk,)))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('admin:billing_paymentmethod_changelist'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('admin:billing_paymentmethod_change', args=(self.payment_method.pk,)))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('admin:billing_membershippurchase_changelist'))
        self.assertEqual(response.status_code, 200)

    def test_admin_invoice_actions(self):
        """
        Test to validate/approve multiple invoices at one time.
        """
        response = self.client.get(reverse('admin:billing_invoice_changelist'))
        self.assertEqual(response.status_code, 200)

        # Validate invoice
        response = self.client.post(reverse('admin:billing_invoice_changelist'), data={
            'action': 'mark_valid',
            'select_across': '0',
            'index': '0',
            '_selected_action': [
                self.invoice.pk,
            ]
        })
        self.assertRedirects(response, reverse('admin:billing_invoice_changelist'))
        self.invoice.refresh_from_db()
        self.assertTrue(self.invoice.valid)

        # Inalidate invoice
        response = self.client.post(reverse('admin:billing_invoice_changelist'), data={
            'action': 'mark_invalid',
            'select_across': '0',
            'index': '0',
            '_selected_action': [
                self.invoice.pk,
            ]
        })
        self.assertRedirects(response, reverse('admin:billing_invoice_changelist'))
        self.invoice.refresh_from_db()
        self.assertFalse(self.invoice.valid)

        # Approve invoice
        response = self.client.post(reverse('admin:billing_invoice_changelist'), data={
            'action': 'mark_approved',
            'select_across': '0',
            'index': '0',
            '_selected_action': [
                self.invoice.pk,
            ]
        })
        self.assertRedirects(response, reverse('admin:billing_invoice_changelist'))
        self.invoice.refresh_from_db()
        self.assertTrue(self.invoice.approved)

        # Disapprove invoice
        response = self.client.post(reverse('admin:billing_invoice_changelist'), data={
            'action': 'mark_disapproved',
            'select_across': '0',
            'index': '0',
            '_selected_action': [
                self.invoice.pk,
            ]
        })
        self.assertRedirects(response, reverse('admin:billing_invoice_changelist'))
        self.invoice.refresh_from_db()
        self.assertFalse(self.invoice.approved)

        # Display add form
        response = self.client.get(reverse('admin:billing_invoice_add'))
        self.assertEqual(response.status_code, 200)

        # Display invoice inline
        response = self.client.get(reverse("admin:auth_user_change", args=(self.admin.pk,)))
        self.assertEqual(response.status_code, 200)

    def test_purchase(self):
        """
        Make a full purchase and ensure that is is correctly working.
        """
        response = self.client.get(reverse('billing:purchase', args=(self.admin.pk,)))
        self.assertEqual(response.status_code, 200)

        # Buy one product
        response = self.client.post(reverse('billing:purchase', args=(self.admin.pk,)), data={
            'payment_method': self.payment_method.pk,
            'form-TOTAL_FORMS': 1,
            'form-INITIAL_FORMS': 0,
            'form-MIN_NUM_FORMS': 1,
            'form-MAX_NUM_FORMS': 1000,
            'form-__prefix__-product': '',
            'form-__prefix__-number': 0,
            'form-__prefix__-id': '',
            'form-0-product': self.product.pk,
            'form-0-number': 1,
            'form-0-id': '',
        })
        self.assertRedirects(response, reverse('billing:invoice_list', args=(self.admin.pk,)))
        qs = Invoice.objects.filter(~Q(pk=self.invoice.pk), Q(target=self.admin))
        self.assertTrue(qs.exists())
        invoice = qs.get()
        self.assertIn(self.product, invoice.products.all())

    @skipIf('member' not in settings.INSTALLED_APPS, "member app is not loaded")
    def test_purchase_membership(self):
        """
        Test to buy a membership, and ensure that we are a member then.
        """
        response = self.client.get(reverse('billing:purchase', args=(self.admin.pk,)))
        self.assertEqual(response.status_code, 200)

        # Ensure that we are not a member
        from member.models import Membership
        self.assertFalse(Membership.objects.filter(member__user=self.admin).exists())

        # Buy membership
        response = self.client.post(reverse('billing:purchase', args=(self.admin.pk,)), data={
            'payment_method': self.payment_method.pk,
            'form-TOTAL_FORMS': 1,
            'form-INITIAL_FORMS': 0,
            'form-MIN_NUM_FORMS': 1,
            'form-MAX_NUM_FORMS': 1000,
            'form-__prefix__-product': '',
            'form-__prefix__-number': 0,
            'form-__prefix__-id': '',
            'form-0-product': self.membership_product.pk,
            'form-0-number': 1,
            'form-0-id': '',
        })
        self.assertRedirects(response, reverse('billing:invoice_list', args=(self.admin.pk,)))
        qs = Invoice.objects.filter(~Q(pk=self.invoice.pk), Q(target=self.admin))
        self.assertTrue(qs.exists())
        invoice = qs.get()
        self.assertIn(self.membership_product, invoice.products.all())

        # Since the invoice is not valid, the membership is not created yet
        self.assertFalse(Membership.objects.filter(member__user=self.admin).exists())

        # Validate invoice
        invoice.valid = True
        invoice.save()

        # Ensure that the membership got created
        self.assertTrue(Membership.objects.filter(member__user=self.admin).exists())
        self.assertIsNotNone(self.admin.profile.current_membership)

        # Invalidate invoice
        invoice.valid = False
        invoice.save()

        # The user is no more a member
        self.assertFalse(Membership.objects.filter(member__user=self.admin).exists())
        self.assertIsNone(self.admin.profile.current_membership)

    def test_render_invoice(self):
        """
        Ensure that invoices render properly.
        """
        response = self.client.get(reverse('billing:render_invoice', args=(self.invoice.pk,)))
        self.assertEqual(response.status_code, 200)

        self.assertFalse(self.invoice.valid)
        self.assertFalse(self.invoice.html)

        self.invoice.valid = True
        self.invoice.save()

        self.assertTrue(self.invoice.valid)
        self.assertTrue(self.invoice.html)

        response = self.client.get(reverse('billing:render_invoice', args=(self.invoice.pk,)))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode('UTF-8'), self.invoice.html)

    def test_comnpay(self):
        """
        Make a purchase with credit card and simulate comnpay requests
        """
        payment_method = ComnpayPaymentMethod.objects.first()
        # Buy one product with credit_card
        response = self.client.post(reverse('billing:purchase', args=(self.admin.pk,)), data={
            'payment_method': payment_method.pk,
            'form-TOTAL_FORMS': 1,
            'form-INITIAL_FORMS': 0,
            'form-MIN_NUM_FORMS': 1,
            'form-MAX_NUM_FORMS': 1000,
            'form-__prefix__-product': '',
            'form-__prefix__-number': 0,
            'form-__prefix__-id': '',
            'form-0-product': self.product.pk,
            'form-0-number': 1,
            'form-0-id': '',
        })
        qs = Invoice.objects.filter(~Q(pk=self.invoice.pk), Q(target=self.admin))
        self.assertTrue(qs.exists())
        invoice = qs.get()
        self.assertRedirects(response, reverse('billing:comnpay_redirect', args=(invoice.pk,)))

        response = self.client.get(reverse('billing:comnpay_redirect', args=(invoice.pk,)))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['comnpay_url'], settings.COMNPAY_URL)

        # Check that signature is valid
        data = response.context['form']
        sec = data['sec']
        del data['sec']
        data['key'] = settings.COMNPAY_SECRET_KEY
        str_with_key = base64.b64encode('|'.join(data.values()).encode('UTF-8'))
        calculated_sec = hashlib.sha512(str_with_key).hexdigest()
        self.assertEqual(sec, calculated_sec)

        # Validate IPN
        data = {
            'result': "OK",
            'idTpe': settings.COMNPAY_ID_TPE,
            'idTransaction': f'{invoice.pk:06d}',
            'montant': f"{invoice.price / 100:.02f}",
            'key': settings.COMNPAY_SECRET_KEY,
        }
        str_with_key = base64.b64encode('|'.join(data.values()).encode('UTF-8'))
        calculated_sec = hashlib.sha512(str_with_key).hexdigest()
        del data['key']
        data['sec'] = calculated_sec
        self.assertFalse(invoice.valid)
        response = self.client.post(reverse('billing:comnpay_ipn'), data=data)
        # Invoice was updated
        self.assertEqual(response.status_code, 204)
        invoice.refresh_from_db()
        self.assertTrue(invoice.valid)
        # Invoice is already valid
        response = self.client.post(reverse('billing:comnpay_ipn'), data=data)
        self.assertEqual(response.status_code, 304)

        # Test errors
        del data['sec']
        response = self.client.post(reverse('billing:comnpay_ipn'), data=data)
        self.assertEqual(response.status_code, 400)
        data['sec'] = calculated_sec
        data['result'] = 'KO'
        response = self.client.post(reverse('billing:comnpay_ipn'), data=data)
        self.assertEqual(response.status_code, 400)
        data['result'] = 'OK'
        data['sec'] = 'toto'
        response = self.client.post(reverse('billing:comnpay_ipn'), data=data)
        self.assertEqual(response.status_code, 400)
        data['idTransaction'] = 'toto'
        data['key'] = settings.COMNPAY_SECRET_KEY
        del data['sec']
        str_with_key = base64.b64encode('|'.join(data.values()).encode('UTF-8'))
        calculated_sec = hashlib.sha512(str_with_key).hexdigest()
        del data['key']
        data['sec'] = calculated_sec
        response = self.client.post(reverse('billing:comnpay_ipn'), data=data)
        self.assertEqual(response.status_code, 400)
        data['idTransaction'] = '999999'
        data['key'] = settings.COMNPAY_SECRET_KEY
        del data['sec']
        str_with_key = base64.b64encode('|'.join(data.values()).encode('UTF-8'))
        calculated_sec = hashlib.sha512(str_with_key).hexdigest()
        del data['key']
        data['sec'] = calculated_sec
        response = self.client.post(reverse('billing:comnpay_ipn'), data=data)
        self.assertEqual(response.status_code, 400)
        data['idTransaction'] = f'{invoice.pk:06d}'
        data['key'] = settings.COMNPAY_SECRET_KEY
        del data['sec']
        str_with_key = base64.b64encode('|'.join(data.values()).encode('UTF-8'))
        calculated_sec = hashlib.sha512(str_with_key).hexdigest()
        del data['key']
        data['sec'] = calculated_sec

        # Success URL
        response = self.client.post(reverse('billing:comnpay_success'))
        self.assertRedirects(response, reverse('index'))

        # Fail URL
        response = self.client.post(reverse('billing:comnpay_fail'), data={'codeReponse': 400, 'reason': "Test"})
        self.assertRedirects(response, reverse('index'))
