from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from polymorphic.models import PolymorphicModel


class Product(PolymorphicModel):
    PAYER_TYPES = [
        ('all', _("All")),
        ('mem', _("Member")),
        ('per', _("Person")),
        ('org', _("Organization")),
        ('cus', _("Custom")),
    ]

    name = models.CharField(
        max_length=255,
        verbose_name=_("name"),
    )
    price = models.PositiveIntegerField(
        verbose_name=_("price"),
    )
    payer_type = models.CharField(
        max_length=3,
        choices=PAYER_TYPES,
        verbose_name=_("payer type"),
    )

    def __str__(self):
        return f"{self.name} ({self.price / 100:.02f} €)"

    class Meta:
        verbose_name = _("product")
        verbose_name_plural = _("products")
        unique_together = ('name', 'payer_type')

    def on_purchase(self, user, entry, valid=True):
        """
        Called when the product is bought.
        """
        pass


class Entry(models.Model):
    invoice = models.ForeignKey(
        'billing.Invoice',
        on_delete=models.CASCADE,
        related_name='entries',
        verbose_name=_("invoice"),
    )
    product = models.ForeignKey(
        'billing.Product',
        on_delete=models.PROTECT,
        related_name='+',
        verbose_name=_("product"),
    )
    number = models.PositiveIntegerField(
        default=1,
        verbose_name=_("number"),
    )
    # price of product can change with time
    price = models.PositiveIntegerField(
        verbose_name=_("price"),
    )

    def save(self, *args, **kwargs):
        if not self.pk or not self.invoice.valid:
            self.price = self.number * self.product.price
        if self.number > 0:
            return super().save(*args, **kwargs)
        else:
            return ()

    def __str__(self):
        return f"{self.number}x {self.product} ({self.price / 100:.02f} €), {self.invoice}"

    class Meta:
        verbose_name = _("entry")
        verbose_name_plural = _("entries")
        unique_together = ('invoice', 'product')


class Invoice(models.Model):
    date = models.DateTimeField(
        verbose_name=_("date"),
        auto_now_add=True,
    )

    target = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        related_name='invoices',
        verbose_name=_("target"),
    )

    payment_method = models.ForeignKey(
        'billing.PaymentMethod',
        on_delete=models.PROTECT,
        related_name='invoices',
        verbose_name=_("payment method"),
    )

    products = models.ManyToManyField(
        Product,
        through=Entry,
        through_fields=('invoice', 'product',),
        verbose_name=_("products"),
    )

    approved = models.BooleanField(
        verbose_name=_("approved"),
        default=False,
    )

    valid = models.BooleanField(
        verbose_name=_("valid"),
        default=False,
    )

    html = models.TextField(
        default="",
        blank=True,
        verbose_name=_("HTML source"),
        help_text=_("We store the HTML source instead of regenerating it each time "
                    "in order to avoid future modifications to invoices."),
    )

    def price(self):
        return sum(entry.price for entry in self.entries.all())
    price.short_description = _("price")
    price = property(price)

    def get_absolute_url(self):
        return reverse_lazy('billing:render_invoice', args=(self.pk,))

    def clean_fields(self, exclude=None):
        if self.valid and not self.pk:
            raise ValidationError({'valid': _("The invoice does not exist yet, it cannot be pre-validated")})
        if self.valid and self.html and self.pk:
            # Check if the invoice was modified while it is not permitted
            # Raise only an error if a field got updated
            saved = Invoice.objects.get(pk=self.pk)
            if any(getattr(self, k) != getattr(saved, k) for k in self.__dict__.keys() if k != '_state'):
                raise ValidationError(_("This invoice is already validated, you can't update it."))
        return super().clean_fields(exclude=exclude)

    def save(self, *args, **kwargs):
        if self.valid and not self.html:
            self.html = render_to_string('billing/invoice_template.html', context={'invoice': self})
        elif not self.valid:
            self.html = ""
        # some actions must be executed upon purchase
        # mostly updating possible memberships
        for entry in self.entries.all():
            entry.product.on_purchase(self.target, entry=entry, valid=self.valid)
        # noinspection PyUnresolvedReferences
        self.target.profile.update_membership()
        super().save(*args, **kwargs)

    def __str__(self):
        return _("invoice #{id}").format(id=self.id).capitalize()

    class Meta:
        verbose_name = _("invoice")
        verbose_name_plural = _("invoices")
