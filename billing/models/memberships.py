from django.db import models
from django.utils.translation import gettext_lazy as _

import datetime

from .invoices import Product
from member.models import Person


class MembershipProduct(Product):
    duration = models.DurationField(
        default=datetime.timedelta(days=366),
        verbose_name=_("duration"),
    )

    def on_purchase(self, user, entry, valid=True):
        """
        Create membership when the product is bought.
        """
        assert type(user.profile) == Person
        for membership in user.profile.membership_purchase.all():
            if membership.purchase_entry == entry:
                if not valid:
                    membership.delete()
                break
        else:
            if valid:
                membership = MembershipPurchase(member=user.profile, purchase_entry=entry)
                membership.save()

    class Meta:
        verbose_name = _("membership product")
        verbose_name_plural = _("membership products")

    def __str__(self):
        return "Membership for {duration} days ({price:.02f} €)".format(
            duration=self.duration.days,
            price=self.price / 100)


class MembershipPurchase(models.Model):
    member = models.ForeignKey(
        'member.Person',
        on_delete=models.PROTECT,
        related_name='membership_purchase',
        verbose_name=_("member"),
    )
    purchase_entry = models.ForeignKey(
        'billing.Entry',
        on_delete=models.PROTECT,
        related_name='membership_purchase',
        verbose_name=_("entry"),
    )

    def __str__(self):
        return f"MembershipPurchase for {self.member}"

    class Meta:
        verbose_name = _("membership purchase")
        verbose_name_plural = _("membership purchases")
