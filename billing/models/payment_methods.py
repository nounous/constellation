from django.db import models
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from polymorphic.models import PolymorphicModel


class PaymentMethod(PolymorphicModel):
    name = models.CharField(
        max_length=255,
        unique=True,
        verbose_name=_("name"),
    )

    visible = models.BooleanField(
        verbose_name=_("visible"),
        help_text=_("if enabled, all members will see this payment method."),
    )

    def get_payment_url(self, invoice):
        """
        URL where the user should be redirected after purchase.
        By default, the invoice stays invalid and we are redirected to the invoice list.
        For a credit card payment, we should be redirected to the payment form.
        """
        return reverse_lazy('billing:invoice_list', args=(invoice.target.pk,))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("payment method")
        verbose_name_plural = _("payment methods")


class ComnpayPaymentMethod(PaymentMethod):
    def get_payment_url(self, invoice):
        return reverse_lazy('billing:comnpay_redirect', args=(invoice.pk,))

    class Meta:
        verbose_name = _("comnpay payment method")
        verbose_name_plural = _("comnpay payment methods")
