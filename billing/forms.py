from django import forms

from billing.models import Entry, Invoice, Product


class EntryForm(forms.ModelForm):
    class Meta:
        model = Entry
        fields = ('product', 'number',)


class CreateInvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = ('payment_method',)


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ('name',)
