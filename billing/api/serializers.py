from rest_framework import serializers

from constellation.serializers import PolymorphicSerializer
from ..models import ComnpayPaymentMethod, Entry, Invoice, PaymentMethod, Product


class PaymentMethodSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentMethod
        fields = '__all__'


class ComnpayPaymentMethodSerializer(serializers.ModelSerializer):
    class Meta:
        model = ComnpayPaymentMethod
        fields = '__all__'


class PaymentMethodPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        PaymentMethod: PaymentMethodSerializer,
        ComnpayPaymentMethod: ComnpayPaymentMethodSerializer,
    }


class EntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Entry
        fields = ('product', 'number', 'price',)


class InvoiceSerializer(serializers.ModelSerializer):
    entries = serializers.ListSerializer(child=EntrySerializer())

    class Meta:
        model = Invoice
        fields = ('id', 'date', 'target', 'payment_method', 'approved', 'valid', 'price', 'entries', 'html',)


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'
