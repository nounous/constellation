from rest_framework.routers import BaseRouter

from . import views


def register_routes(router: BaseRouter) -> None:
    router.register('billing/invoice', views.InvoiceViewSet)
    router.register('billing/payment-method', views.PaymentMethodViewSet)
    router.register('billing/product', views.ProductViewSet)
