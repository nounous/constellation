from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.filters import OrderingFilter

from .serializers import InvoiceSerializer, PaymentMethodPolymorphicSerializer, ProductSerializer
from ..models import Invoice, PaymentMethod, Product


class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter,)
    filterset_fields = ('date', 'target', 'target__username', 'payment_method', 'payment_method__name',
                        'products', 'approved', 'valid',)
    ordering_fields = ('date',)


class PaymentMethodViewSet(viewsets.ModelViewSet):
    queryset = PaymentMethod.objects.all()
    serializer_class = PaymentMethodPolymorphicSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('name', 'polymorphic_ctype',)


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('name', 'price', 'payer_type',)
