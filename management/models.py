from django.db import models
from django.utils.translation import gettext_lazy as _


class ManagedInterface(models.Model):
    interface = models.OneToOneField(
        'layers.Interface',
        related_name='domains',
        on_delete=models.PROTECT,
    )
    a_records = models.ManyToManyField(
        'dnsmanager.AddressRecord',
        blank=True,
        related_name='+',
        verbose_name=_("A records"),
    )
    aaaa_records = models.ManyToManyField(
        'dnsmanager.Ipv6AddressRecord',
        blank=True,
        related_name='+',
        verbose_name=_("AAAA records"),
    )
    cname_records = models.ManyToManyField(
        'dnsmanager.CanonicalNameRecord',
        blank=True,
        related_name='+',
        verbose_name=_("CNAME records"),
    )
    ptr_records = models.ManyToManyField(
        'dnsmanager.PointerRecord',
        blank=True,
        related_name='+',
        verbose_name=_("PTR records"),
    )
    firewall_layer2 = models.ManyToManyField(
        'firewall.FireWallLayer2',
        blank=True,
        related_name='+',
        verbose_name=_("layer 2 firewalls"),
    )
    firewall_layer3 = models.ManyToManyField(
        'firewall.FireWallLayer3',
        blank=True,
        related_name='+',
        verbose_name=_("layer 3 firewalls"),
    )
    firewall_layer4 = models.ManyToManyField(
        'firewall.FireWallLayer4',
        blank=True,
        related_name='+',
        verbose_name=_("layer 4 firewalls"),
    )

    def __str__(self):
        return _("managed interface for {interface}").format(interface=self.interface)

    class Meta:
        verbose_name = _("managed interface")
        verbose_name_plural = _("managed interfaces")
