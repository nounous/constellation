FROM debian:bullseye-slim

# Force the stdout and stderr streams to be unbuffered
ENV PYTHONUNBUFFERED 1

# Install Django, external apps, LaTeX and dependencies
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    python3-django python3-django-crispy-forms python3-django-extensions \
    python3-django-filters python3-django-polymorphic \
    python3-djangorestframework python3-docutils python3-django-tables2 \
    python3-pip uwsgi uwsgi-plugin-python3 && \
    rm -rf /var/lib/apt/lists/*

# Instal PyPI requirements
COPY requirements.txt /var/local/constellation/
RUN pip3 install -r /var/local/constellation/requirements.txt --no-cache-dir

# Copy code
WORKDIR /var/local/constellation
COPY . /var/local/constellation/

EXPOSE 8080
ENTRYPOINT ["/var/local/constellation/entrypoint.sh"]
