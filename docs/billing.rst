Billing - Produits et factures
==============================

Modèles
-------

``Product``: un produit proposé à l'achat (e.g. une adhésion ou un produit matériel)

``Entry``: un achat de plusieurs produits identiques

``Invoice``: une collection datée de plusieurs ``Entry`` achetés en même temps

``MembershipProduct``: une catégorie spéciale de ``Product`` qui représente
une adhésion

``MembershipPurchase``: un achat d'une adhésion enregistré pour régénération
plus efficace des périodes d'adhésions (voir `Sémantique des adhésions <member.html>`_).
Ce modèle est une table de cache.

``PaymentMethod``: une méthode de payement (carte, espèces, chèque)

``ComnpayPaymentMethod``: méthode de payement particulière par carte


Création d'une nouvelle facture
-------------------------------

Attention : lors d'un achat quelconque une facture est créée, mais elle n'est pas toujours validée.

Plus précisément chaque facture peut être "validée" et "approuvée", le premier signifiant qu'elle
a été débitée et que le(s) produit(s) acheté(s) sont comptabilisés, le second indiquant qu'un
administrateur a manuellement vérifié que l'état de la facture (validée ou non) était le bon.

Un achat par une méthode de payement en ligne résultera en une facture validée mais non approuvée.
Un règlement par chèque ou par espèces devra être manuellement validé et approuvé par un
utilisateur qui a ce privilège. Une facture approuvée mais non validée signifie qu'un
administrateur a décidé de rendre cette facture invalide.

Une adhésion sera prise en compte lorsque la facture associée deviendra "validée", il est normal
qu'un utilisateur ne soit pas automatiquement rendu adhérent si le payement se fait par espèces.

Pour valider (et approuver) une facture, se rendre dans l'interface administrateur, sélectionner
``Invoices``, puis cocher les deux cases ``[X] approved`` et ``[X] valid``.

Il n'est pas possible de valider une facture qui n'existe pas encore, donc la création manuelle
d'une facture doit être faite en deux étapes avec un enregistrement entre temps :
ajout des produits, puis validation.
De manière similaire une facture validée ne peut pas être éditée, donc une modification du
destinataire ou des produits doit être précédée de l'invalidation manuelle de la facture.


Regénération des adhésions
--------------------------

Dès qu'une facture est validée (resp. invalidée), si elle contient un achat d'une adhésion un
objet associé qui enregistre la date d'achat de l'adhésion et l'utilisateur est créé (resp.
supprimé si il existe).

Cette modification provoque un évènement qui supprime toutes les adhésions enregistrées
pour la personne concernée, puis reconstruit de manière déterministe les périodes d'adhésion
à partir des enregistrements d'achats d'adhésions par la personne.

Il y a ainsi deux tables de cache intermédiaires, ``MembershipPurchase`` dans ``billing`` et
``Membership`` dans ``member`` qui permettent de déterminer efficacement qui est adhérent
sans nécessiter un parcours de toutes les factures à chaque fois.

Puisque le test de validité de l'adhésion est fait à plusieurs reprises sur les pages de gestion
de compte et d'achat de produits, il est important que cette opération soit efficace.

