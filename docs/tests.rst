Exécution des tests
===================

``tox`` est un outil permettant de configurer l'exécution des tests. Ainsi, après
installation de tox dans votre environnement virtuel via ``pip install tox``,
il vous suffit d'exécuter ``tox -e py39-django32`` pour lancer les tests avec
Python 3.9 et Django 3.2, et ``tox -e linters`` pour vérifier la syntaxe du code.

Tests unitaires
---------------

Django gère nativement ses tests. En lançant la commande
``./manage.py test <app1> ... <appn>`` (ou ``tox -e py39-django32`` pour ne
pas s'embêter et s'assurer d'être dans un environnement valide), les tests s'exécutent.

Chaque application est testée indépendamment. Les tests d'une application sont situés
dans le module ``app.tests``. Ce module est composé d'une succession de classes
héritant de ``django.test.TestCase``. Chacune de ces classes dispose d'une multitude
de fonctions préfixées par ``test_``. Ces fonctions sont appelées *tests unitaires*.

Pour chacune de ces fonctions, une base de données dédiée sera créée, afin d'avoir des
tests au maximum indépendants. Pour générer un jeu de données commun à plusieurs
tests, on peut créer une fonction ``setUp``, qui sera appelée avant chacun des tests.

Au cours de ces tests, on dispose d'un client fictif qui peut effectuer des requêtes.
Le fait d'effectuer ces requêtes assure qu'il n'y a pas d'erreur levée pendant son
traitement. Cependant, rien ne garantit que la requête a fonctionné comme prévu.
Il faut donc s'assurer que l'état après la soumission de la requête est bien celui voulu.

Par exemple, si l'on veut tester que la mise à jour de son adresse fonctionne bien
après l'avoir modifié via un formulaire dédié :

.. code:: python

   class TestEmailAddress(TestCase):
       def setUp(self):
           # On crée un nouvel utilisateur avec lequel on va se connecter
           self.user = User.objects.create(username="toto", email="test@example.com")
           self.client.force_login(self.user)

       def test_change_email_address(self):
           # On vérifie qu'on arrive déjà à lire le formulaire
           # On suppose que `change_my_email_address` est le nom de la vue qui
           # permet de changer d'adresse e-mail
           response = self.client.get(reverse('change_my_email_address'))
           # OK
           self.assertEqual(response.status_code, 200)

           # On soumet des données au formulaire
           response = self.client.post(reverse('change_my_email_address'), data={
               email="toto@example.com",
           })
          # Un formulaire bien soumis entraîne une redirection
          self.assertRedirects(response, reverse('my_user_detail'))
          # On récupère les dernières infos de l'utilisateur
          self.user.refresh_from_db()
          # On vérifie que l'adresse e-mail a bien été modifiée
          self.assertEqual(self.user.email, "toto@example.com")

Il est également possible (et fortement conseillé) de tester le comportement des
formulaires avec des données invalides.

Les tests peuvent sembler lourds et souvent nécessiter bien plus de lignes qu'il n'en
a nécessité pour implémenter la fonctionnalité. Ce n'est pas un mal, ils peuvent
rester là. Sauf si obsolète, un test n'est jamais inutile. Avoir de la redondance
dans les informations testées n'est pas forcément grave non plus.

Il faut néanmoins garder à l'esprit qu'un test doit rester unitaire, c'est-à-dire tester
une seule chose à la fois. Il serait laborieux de prouver formellement que le code
fonctionne comme il devrait, les tests automatiques sont donc la seule chose qui
permettent d'assurer le bon fonctionnement du projet. Si leur écriture peut prendre
du temps, c'est rarement du temps perdu et aide assez souvent au débuggage lorsqu'une
fonctionnalité se retrouve cassée.

En plus d'assurer le bon fonctionnement du site, les tests calculent également le taux
de couverture du code, c'est-à-dire l'ensemble des lignes qui ont été interprétées par
rapport aux autres. Il est essentiel de chercher à couvrir le plus de code utile.
Néanmoins, les tests arrivant à cela ne doivent pas être de préférence trop débiles.

Plus d'informations dans la documentation officielle de Django :
`<https://docs.djangoproject.com/fr/3.2/topics/testing/>`_.

Ces tests sont exécutés à chaque commit par l'intégration continue.


Analyseur syntaxique
--------------------

``flake8`` est utilisé en guise d'analyseur syntaxique. Il vérifie si le code
est bien formatté afin d'assurer une certaine lisibilité. En particulier,
il vérifie l'indentation, si chaque variable est bien utilisée, s'il n'y a pas
d'import inutile et s'ils sont dans l'ordre lexicographique ou encore si chaque
ligne fait au plus 120 caractères.

Pour lancer l'analyse, ``tox -e linters`` suffit. L'intégration continue
effectue cette analyse à chaque commit.
