Administrer Constellation
=========================

L'administration de Constellation se fait essentiellement via une interface Django-admin.
Cette interface permet de gérer l'ensemble des objets présents en base de données,
ergonomiquement et simplement. Cette interface n'est pas conçue pour être accessible par
n'importe qui, des droits sont requis.

Pour un modèle donné, il est possible de voir, ajouter, modifier, supprimer toutes les
instances (qui peuvent être filtrées), selon permissions, avec éventuellement ses relations
avec d'autres objets.

La documentation complète de Django-admin peut être trouvée ici :
`<https://docs.djangoproject.com/fr/3.2/ref/contrib/admin/>`_.


Authentification et autorisation
--------------------------------

Utilisateurs
~~~~~~~~~~~~

Il s'agit du modèle des comptes utilisateurs, natif à Django.

Il contient les paramètres de base :

* Pseudo
* Formulaire de changement de mot de passe
* Prénom
* Nom de famille
* Adresse électronique
* Statut actif/équipe/super-utilisateur
* Groupes et permissions individuelles (non utilisé pour le moment)
* Dates d'inscription et de dernière connexion

Un bouton « Usurper l'identité » a été ajouté pour les super-utilisateurs afin de pouvoir
voir à quoi ressemble le site en tant qu'un compte spécifié, essentiellement pour du débuggage.

En plus de ces informations, les applications peuvent définir une liste de
sous-formulaires supplémentaires appelés *inlines* associés à l'utilisateur.

L'application membres ajoute par exemple un formulaire pour gérer l'objet profil associé
à l'utilisateur, qui peut être soit une personne soit une association. Tous les paramètres
peuvent être modifiés directement.

De plus, l'application factures affiche directement sur la page de l'utilisateur l'ensemble
de ses propres factures. Il n'est pas possible de les modifier directement, seulement de les
valider/approuver.

.. image:: /_static/img/user_inlines.png
   :alt: Plusieurs inlines

.. warning::

   Cette fonctionnalité n'est disponible que si vous utilisez au moins Django 3.0.
   Sous Django 2.2, ces sous-formulaires n'apparaîtront pas.


Groupes
~~~~~~~

Ce modèle est natif à Django est n'est pas utilisé par Constellation, pour le moment.


