Installer Constellation en production
=====================================

Cette page détaille comment installer Constellation sur un serveur de production,
dédié uniquement à l'utilisation de Constellation. On supposera que le serveur tourne
avec un Debian Bullseye à jour.


Ajout des dépôts bullseye-backports
-----------------------------------

Debian c'est bien, c'est stable, mais les paquets sont vite obsolètes.
En particulier, la version stable de Django dans la version à venir de Debian est la
version 2.2, qui n'est pas la dernière version stable de Django,
à savoir la version 3.2.

Afin de permettre à ses utilisateurs d'utiliser les dernières fonctionnalités de
certains paquets, Debian a créé une distribution particulière, appelée
``bullseye-backports``. Cette distribution contient des paquets de la distribution
à venir « ``testing`` » (``bookworm`` à l'heure où cette documentation est écrite)
recompilés et réadaptés pour fonctionner avec les paquets de la distribution stable
(``bullseye``). Ce qui nous intéresse est de pouvoir récupérer Django 3.2 depuis
cette distribution, et avoir donc une version à jour de Django.

Plus de détails sur le wiki de Debian : `<https://wiki.debian.org/fr/Backports>`_.

Pour activer les backports, il suffit d'ajouter dans le fichier
``/etc/apt/sources.list`` :

.. code::

   deb     $MIRROR/debian bullseye-backports main contrib

où ``$MIRROR`` est votre miroir Debian favori, comme ``http://ftp.debian.org`` ou
``http://mirror.crans.org`` pour ce qui est de la version utilisée en production au Crans.
Il suffit ensuite de faire un ``sudo apt update``.
Vérifiez que les paquets sont bien récupérés, en cherchant cette ligne :

.. code::

   Get:4 http://mirror.crans.org/debian bullseye-backports InRelease [46.7 kB]

.. warning::

   Cette documentation a fait un petit saut dans le futur. En effet : Debian Bullseye
   n'étant pas encore sorti, ``bullseye-backports`` n'existe pas encore, ni
   ``bookworm``. Pour récupérer Django 3.2 depuis un Debian Bullseye, il faut
   pour l'instant aller le chercher dans la distribution ``experimental`` et
   non ``bullseye-backports``.

   Afin de ne pas polluer son serveur avec uniquement des dépendances expérimentales,
   on ajoute un fichier ``/etc/apt/preferences.d/django-experimental`` contenant :

   .. code::

      Package: *
      Pin: release n=experimental
      Pin-Priority: 1

      Package: python3-django
      Pin: release n=experimental
      Pin-Priority: 900

      Package: python3-django-tables2
      Pin: release n=experimental
      Pin-Priority: 900

   Cela a pour effet de ne pas prioriser la distribution expérimentale pour la plupart
   des paquets, sauf ``python3-django`` et ``python3-django-tables2``.


Installation des dépendances nécessaires
----------------------------------------

Dépendances APT
~~~~~~~~~~~~~~~

On s'efforce pour récupérer le plus possible de dépendances via les paquets Debian
plutôt que via ``pip`` afin de faciliter les mises à jour et avoir une installation
plus propre. On peut donc installer tout ce dont on a besoin :

.. code:: bash

   $ sudo apt update
   $ sudo apt install --no-install-recommends \
       gettext git ipython3 \  # Dépendances basiques
       python3-django python3-django-extensions python3-django-polymorphic \
       python3-ipython python3-pip python3-psycopg2

Ces paquets fournissent une bonne base sur laquelle travailler.

Si l'on veut que l'affichage Web fonctionne (non essentiel si l'on veut travailler uniquement
avec des scripts qui interagissent avec la base de données) :

.. code:: bash

   $ sudo apt install --no-install-recommends \
       python3-django-crispy-forms python3-django-filters python3-djangorestframework \
       python3-django-tables2 python3-docutils \
       python3-sphinx python3-sphinx-rtd-theme  # Pour générer la doc

Pour les mettre à jour, il suffit de faire ``sudo apt update`` puis ``sudo apt upgrade``.


Dépendances PIP
~~~~~~~~~~~~~~~

Certaines dépendances utiles n'existent malheureusement pas dans APT, on doit donc les installer
via PIP. C'est le cas de deux dépendances, dont une utilisée uniquement si un serveur Web est
rattaché. Pour les installer :

.. code:: bash

   $ sudo pip3 install git+https://gitlab.crans.org/nounous/django-dnsmanager.git
   $ sudo pip3 install git+https://gitlab.crans.org/nounous/crispy-bootstrap5.git

``django-dnsmanager`` n'est utile que si la gestion des serveurs DNS est voulue.
``crispy-bootstrap5`` n'est utile que si vous souhaitez activer un serveur web.
Le lien donné n'est qu'un miroir local, le dépôt officiel étant situé à l'adresse
`<https://github.com/django-crispy-forms/crispy-bootstrap5>`_.

Pour les mettre à jour, rajouter l'option ``--upgrade``.


Téléchargement de Constellation
-------------------------------

On utilise directement le Gitlab du Crans pour récupérer les sources et les installer.

Sur un serveur de production
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sur un serveur de production, il suffit d'installer directement le projet via pip :

.. code:: bash

   $ sudo pip3 install git+https://gitlab.crans.org/nounous/constellation.git

Si l'on souhaite s'assurer que l'on dispose bien des dépendances souhaitées pour
le serveur web, l'option ``[front]`` peut être ajoutée.

Les sources du code sont donc naturellement téléchargées dans
``/usr/local/lib/python3.9/dist-packages/constellation``, et même chose pour les sous-applications.


Sur un serveur de pré-production
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si le serveur est destiné à pouvoir accueillir du développement, il peut être préférable
de cloner à la main le dépôt. On suppose que l'on veut cloner le dépôt dans
``/var/local/constellation`` et récupérer la branche ``dev`` :

.. code::

   $ sudo git clone -b dev https://gitlab.crans.org/nounous/constellation.git /var/local/constellation
   $ cd /var/local/constellation
   $ sudo pip3 install -e .

La dernière étape permet d'installer le dépôt via PIP avec le drapeau ``editable``, qui permet
à Python de dire que la dépendance est dans ce dossier et peut donc être modifiée à souhait.

Dans les deux cas, l'installation fournit un exécutable nommé ``constellation`` placé
dans ``/usr/local/bin`` pointant vers le fichier ``manage.py``.


Configuration de Constellation
------------------------------

La configuration de Constellation se gère via un fichier de paramètres locaux placé dans
``constellation/settings_local.py``.

Pour avoir une installation propre, on peut placer le fichier de configuration dans
``/etc/constellation/settings_local.py`` puis créer un lien symbolique, selon le cas :

.. code::

   $ sudo ln -s /etc/constellation/settings_local.py /usr/local/lib/python3.9/dist-packages/constellation/settings_local.py

Ou bien :

.. code::

   $ sudo ln -s /etc/constellation/settings_local.py /var/local/constellation/settings_local.py

Un exemple de ce fichier peut être :

.. code:: python

    # A secret key used by the server.
    SECRET_KEY = "CHANGE_ME"

    # Should the server run in debug mode ?
    # SECURITY WARNING: don't run with debug turned on in production!
    DEBUG = False

    # A list of admins of the services. Receive mails when an error occurs
    ADMINS = [('Root', 'root@crans.org'), ]

    # The list of hostname the server will respond to.
    ALLOWED_HOSTS = ['constellation.crans.org', ]

    # Installed applications
    LOCAL_APPS = [
        'access',
        'billing',
        'dnsmanager',
        'firewall',
        'layers',
        'management',
        'member',
        'topography',
        'unix',
    ]

    # Activate this option if a web front is needed
    USE_FRONT = True

    # The time zone the server is runned in
    TIME_ZONE = 'Europe/Paris'

    # The storage systems parameters to use
    DATABASES = {
        'default': {  # The DB
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'constellation',
            'USER': 'constellation',
            'PASSWORD': "CHANGE_ME",
            'HOST': 'pgsql.adm.crans.org',
            'PORT': '5432',
        },
    }


    # The mail configuration for Constellation to send mails
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_USE_SSL = False
    EMAIL_HOST = 'smtp.adm.crans.org'
    EMAIL_PORT = 25
    EMAIL_HOST_USER = ''
    EMAIL_HOST_PASSWORD = ''
    SERVER_EMAIL = 'root@crans.org'
    DEFAULT_FROM_EMAIL = 'Crans <root@crans.org>'

    COMNPAY_ID_TPE = 'CHANGE_ME'
    COMNPAY_SECRET_KEY = 'CHANGE_ME'

Le champ ``SECRET_KEY`` est utilisé pour la protection CSRF (voir la documentation
`<https://docs.djangoproject.com/fr/3.2/ref/csrf/>`_ pour plus de détails). Il s'agit d'une
clé sous forme de chaîne de caractère suffisamment longue (64 caractères paraît bien)
qui n'est pas à transmettre et qui évite d'autres sites malveillants de faire des requêtes
directement depuis leur propre site.

La variable ``DEBUG`` indique si les messages d'erreur doivent s'afficher directement ou s'ils
doivent être envoyés par mail, aux administrateurs (champ suivant).

Le tableau ``ALLOWED_HOSTS`` contient la liste des noms de domaine autorisés à utiliser le site.

La liste ``LOCAL_APPS`` contient l'ensemble des applications de constellation chargées.
Le schéma de dépendances étant très faible, quasiment tout peut être désactivé à souhait.

Si la variable ``USE_FRONT`` vaut ``False``, alors le support de l'affichage web sera désactivé,
et toutes les dépendances nécessaires au web ne seront pas chargées.

Le dictionnaire ``DATABASES`` permet de configurer la connexion à la base de données. On supposera
la base de données déjà installée et créée avec les bonnes permissions.

Les champs en ``EMAIL_`` permettent de configurer l'envoi de mails, avec les paramètres SMTP.
Pour un serveur de test, on peut vouloir afficher les mails dans les logs plutôt qu'effectivement
envoyer les mails. Pour cela, il suffit de remplacer l'option :

.. code:: python

   EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

Les paramètres de ComnPay permettent d'utiliser le TPE en ligne.
En mode DEBUG, le site d'homologation de ComnPay est utilisé.


Configuration des tâches récurrentes
------------------------------------

Certaines opérations se font périodiquement. On utilise pour cela un cron.
Il suffit pour cela de copier le fichier ``constellation.cron``
vers ``/etc/cron.d/constellation``, en veillant à ce qu'il appartienne bien à ``root``.

Ce fichier contient l'ensemble des tâches récurrentes associées à Constellation.

Sur un serveur de pré-production, on peut ne pas souhaiter activer ces tâches récurrentes.


Finir l'installation de Django
------------------------------

On commence par construire la base de données à partir des migrations enregistrées :

.. code:: bash

   $ constellation migrate

On doit compiler les traductions (pour pouvoir les lire plus vite par la suite) :

.. code:: bash

   $ constellation compilemessages

Les fichiers statiques (fiches de style, fichiers Javascript, images, ...) doivent
être exportées dans le dossier ``static``, si l'on utilise un serveur Web.

Toutefois, on souhaite afin d'avoir une installation propre avoir ces fichiers placés
dans ``/var/lib/constellation/static``. En pré-production, on peut commencer par créer
les liens symboliques qui va bien, et de même pour les fichiers envoyés :

.. code:: bash

   $ sudo -u www-data mkdir -p /var/local/constellation/static
   $ sudo -u www-data mkdir -p /var/local/constellation/media
   $ sudo ln -s /var/local/constellation/static /var/local/constellation/static
   $ sudo ln -s /var/local/constellation/media /var/local/constellation/media

En production, on peut plutôt indiquer dans le fichier de configuration l'endroit où
chercher ces fichiers statiques :

.. code:: python

   ...
   STATIC_ROOT = "/var/lib/constellation/static/"
   MEDIA_ROOT = "/var/lib/constellation/media/"
   ...

Et on peut enfin récupérer les fichiers statiques :

.. code:: bash

   $ constellation collectstatic

Et on peut enfin importer certaines données de base :

.. code:: bash

   $ constellation loaddata initial

Constellation est désormais prêt à être utilisée.
Ne reste qu'à configurer un serveur Web si besoin.


Configuration de UWSGI
----------------------

On dispose d'une instance de Constellation fonctionnelle et bien configurée.
On peut déjà interagir avec via le point d'entrée ``constellation``. Cependant, nous
n'avons pas encore de socket permettant d'intéragir avec le serveur. C'est le travail
de UWSGI.

On rappelle que la commande ``./manage.py runserver`` n'est pas conçue pour des serveurs
de production, contrairement à UWSGI.

On commence par installer UWSGI :

.. code:: bash

   $ sudo apt install --no-install-recommends uwsgi uwsgi-plugin-python3

On place ensuite le fichier de configuration UWSGI dans les applications installées.
Un bon exemple de fichier à placer dans ``/etc/uwsgi/apps-available/constellation.ini`` :

.. code:: ini

    [uwsgi]
    uid             = www-data
    gid             = www-data
    # Django-related settings
    # the base directory (full path)
    chdir           = /usr/local/lib/python3.9/dist-packages/constellation
    wsgi-file       = /usr/local/lib/python3.9/dist-packages/constellation/wsgi.py
    plugin          = python3
    # process-related settings
    # master
    master          = true
    # maximum number of worker processes
    processes       = 10
    # the socket (use the full path to be safe
    socket          = /var/run/uwsgi/app/constellation/constellation.sock
    # ... with appropriate permissions - may be needed
    chmod-socket    = 664
    # clear environment on exit
    vacuum          = true
    # Touch reload
    touch-reload = /usr/local/lib/python3.9/dist-packages/constellation/settings.py

On peut désormais activer l'application et relancer UWSGI :

.. code:: bash

   $ sudo ln -s /etc/uwsgi/apps-available/constellation.ini /etc/uwsgi/apps-enabled/constellation.ini
   $ sudo systemctl restart uwsgi


Configuration de NGINX
----------------------

Nous avons désormais un socket qui nous permet de faire des connexions au serveur web,
placé dans ``/var/run/uwsgi/app/constellation/constellation.sock``. Cependant, ce socket
n'est pas accessible au reste du monde, et ne doit pas l'être : on veut un serveur Web
Nginx qui s'occupe des connexions entrantes et qui peut servir de reverse-proxy,
notamment utile pour desservir les fichiers statiques ou d'autres sites sur le même serveur.

On commence donc par installer Nginx :

.. code:: bash

   $ sudo apt install nginx

On place ensuite dans ``/etc/nginx/sites-available/constellation`` le fichier de
configuration Nginx qui va bien, en remplaçant ``constellation.crans.org`` par ce qu'il faut :

.. code::

    # Automatic Connection header for WebSocket support
    # See http://nginx.org/en/docs/http/websocket.html
    map $http_upgrade $connection_upgrade {
        default upgrade;
        ''      close;
    }

    upstream constellation {
        # Path of the server
        server unix:///var/run/uwsgi/app/constellation/constellation.sock;
    }
    server {
        listen 80 default;
        listen [::]:80 default;
        server_name constellation.crans.org intranet.crans.org;
        charset utf-8;

        # Hide Nginx version
        server_tokens off;

        location /static {
            alias /var/lib/constellation/static/;
        }

        location /media {
            alias /var/lib/constellation/media/;
        }

        location /doc {
            alias /var/www/constellation-doc/;
        }

        location / {
            uwsgi_pass constellation;
            include /etc/nginx/uwsgi_params;
        }
    }

On peut enfin activer le site :

.. code::

   $ sudo ln -s /etc/nginx/sites-available/constellation /etc/nginx/sites-enabled/constellation

Nginx écoutera ensuite sur son port 80. Ne reste donc plus qu'à ajouter un point
d'entrée HTTPS sur son reverse-proxy favori.


Mettre à jour Constellation
---------------------------

Pour mettre à jour Constellation, il suffit a priori de mettre à jour les paquets APT
et les paquets PIP, incluant celui de Constellation (peut nécessiter un ``git pull``
pour un serveur de pré-production).

Les éventuelles nouvelles migrations de la base de données doivent être appliquées :

.. code:: bash

   $ constellation migrate

Les nouvelles traductions compilées :

.. code:: bash

   $ constellation compilemessages

Les nouveaux fichiers statiques collectés : (si serveur Web configuré)

.. code:: bash

   $ constellation collectstatic

Et enfin les nouvelles fixtures installées :

.. code:: bash

   $ constellation loaddata initial

Une fois tout cela fait, il suffit de relancer le serveur UWSGI au besoin :

.. code:: bash

   $ sudo systemctl restart uwsgi


Avec Ansible
------------

Un playbook Ansible a été écrit afin d'automatiser toutes les tâches d'installation.
Il est présent sur le dépôt des recettes Ansible du Crans :
`<https://gitlab.crans.org/nounous/ansible>`_, dans le playbook ``constellation``.

Sa configuration à donner, avec ses paramètres par défaut :

.. code:: yaml

    ---
    constellation:
      django_secret_key: "CHANGE_ME"
      admins:
        - ('Root', 'root@crans.org')
      allowed_hosts:
        - 'constellation.crans.org'
        - 'intranet.crans.org'
      email:
        ssl: false
        host: "smtp.adm.crans.org"
        port: 25
        user: ''
        password: ''
        from: "root@crans.org"
        from_full: "Crans <root@crans.org>"
      database:
        host: "pgsql.adm.crans.org"
        port: 5432
        user: 'constellation'
        password: "CHANGE_ME"
        name: 'constellation'
      front: true
      crontab: true
      applications:
        - 'access'
        - 'billing'
        - 'dnsmanager'
        - 'firewall'
        - 'layers'
        - 'management'
        - 'member'
        - 'topography'
        - 'unix'
      comnpay:
        tpe: 'CHANGE_ME'
        secret: 'CHANGE_ME'
      debug: false
      owner: root
      group: nounou
      version: master
      settings_local_owner: www-data
      settings_local_group: nounou

    nginx:
      service_name: constellation
      ssl: []
      servers:
        - ssl: false
          default: true
          server_name:
            - "constellation.crans.org"
            - "intranet.crans.org"
          locations:
            - filter: "/static"
              params:
                - "alias /var/lib/constellation/static/"

            - filter: "/media"
              params:
                - "alias /var/local/constellation/media/"

            - filter: "/doc"
              params:
                - "alias /var/www/constellation-doc/"

            - filter: "/"
              params:
                - "uwsgi_pass constellation"
                - "include /etc/nginx/uwsgi_params"
      upstreams:
        - name: 'constellation'
          server: 'unix:///var/run/uwsgi/app/constellation/constellation.sock'


Lancer le playbook assure que l'installation est en place, et se permet également
de mettre à jour Constellation au besoin en pensant à appliquer les migrations au
besoin.
