Documentation
=============

La documentation est gérée grâce à Sphinx. Le thème est le thème officiel de
ReadTheDocs ``sphinx-rtd-theme``.

Générer localement la documentation
-----------------------------------

On commence par se rendre au bon endroit et installer les bonnes dépendances :

.. code:: bash

  cd docs
  pip install -r requirements.txt

Ou bien via APT :

.. code:: bash

   sudo apt install python3-sphinx python3-sphinx-rtd-theme

La documentation se génère à partir d'appels à ``make``, selon le type de
documentation voulue.

Par exemple, ``make dirhtml`` construit la documentation web,
``make latexpdf`` construit un livre PDF avec cette documentation.


Documentation automatique
-------------------------

Ansible compile et déploie automatiquement la documentation du projet, dans
le rôle ``constellation-doc``. Le rôle installe les dépendances
nécessaires, puis appelle sphinx pour placer la documentation compilée dans
``/var/www/constellation-doc`` :

.. code:: bash

   sphinx-build -b dirhtml /var/local/constellation/docs/ /var/www/constellation-doc/

Ce dossier est exposé par ``nginx`` sur le chemin
`/doc <https://constellation-dev.crans.org/doc>`_.
