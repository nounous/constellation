Constellation, c'est quoi ?
===========================

Constellation est une plateforme modulaire développée par le Crans permettant la
gestion d'une infrastructure réseau, sous de nombreux aspects allant de la gestion
des adhésions et factures de l'association jusqu'à la gestion des machines et des
différents sous-réseaux.

À l'heure actuelle, seule la gestion des adhérents et des factures a été implémentée,
documentée et testée.

Cette plateforme a été développée grâce au framework
`Django <https://www.djangoproject.com/>`_ écrit en Python. Les version 3.6 à 3.9
de Python sont supportées et testées, ainsi que les versions 2.2 à 3.2 de Django.
Il est recommandé d'utiliser Django 3.2 avec Python 3.9.

Une instance de développement est accessible à `<https://constellation-dev.crans.org/>`_.


Installation
------------

L'installation de Constellation est détaillée sur une page dédiée :
`en production <install.html>`_, `en développement <install-dev.html>`_


Utilisation, en tant qu'adhérent⋅e
----------------------------------

Constellation peut être utilisé par tout⋅e adhérent⋅e pour gérer son compte et
ses adhésions.

S'inscrire
~~~~~~~~~~

Pour commencer, il est possible de s'inscrire en cliquant sur le bouton dans
le menu. Les informations suivantes sont demandées :

 * Nom
 * Prénom
 * Adresse électronique
 * Pseudo (contenant uniquement des lettres minuscules, des chiffres et des tirets)
 * Mot de passe (à taper 2 fois, doit être suffisamment complexe)
 * Adresse
 * Numéro de téléphone

Il est inutile de créer plusieurs comptes. À l'inscription, il est donc demandé de
cocher une case attestant que vous n'avez pas déjà créé un compte. Vous devrez
également avoir lu et approuvé les conditions générales d'utilisation propres à
l'association.

Vous recevrez ensuite un e-mail vous demander de confirmer votre adresse électronique.

Votre compte est ensuite activé, et vous pouvez vous connecter et accéder à votre compte.

Adhérer
~~~~~~~

Pour profiter des services de l'association, il est essentiel d'adhérer. Pour cela,
rendez-vous dans votre compte, et cliquez sur le bouton « Acheter une
adhésion/connexion/un produit » (ou bien « Adhérer maintenant » si vous n'êtes pas
encore adhérent). Vous pouvez ensuite sélectionner une adhésion, puis
payer par carte bancaire. S'il ne vous est pas possible de payer par carte, contactez-nous
pour régler par le mode de votre choix.

Une fois l'adhésion payée, vous êtes désormais adhérent⋅e pour la durée indiquée, et
la période de validité de votre adhésion est visible sur votre profil.
Dans les minutes qui suivent, les services de l'association seront ensuite accessibles.

Sur votre compte, il vous est possible de voir votre adhésion en cours et de consulter
vos factures passées. Vous n'avez pas besoin d'attendre que votre adhésion expire
pour la renouveller : la date de début de validité de la nouvelle adhésion est
comptée à partir de l'instant de fin de validité de celle en cours.


Utilisation, en tant qu'administrateur⋅rice
-------------------------------------------

L'interface d'administration est entièrement gérée par Django-admin, il n'y a pas
d'interface personnalisée.

Pour accéder à l'interface, rendez-vous sur la page `/admin </admin/>`_. Vous devez
pour cela avoir des privilèges d'administration.

Sur cette interface, il est possible de gérer l'ensemble des objets de la base de
données, triés par application. Les formulaires sont conçus pour être ergonomiques
et faciles d'utilisation, afin de rendre faciles, rapides et accessibles tous les
accès ou modifications souhaités.

À titre d'exemple, sur la page d'un utilisateur, l'ensemble des informations liées
à l'utilisateur sont affichées, y compris les factures et les adhésions.

Plus de détails sur la page `d'administration <admin>`_.

La documentation complète de Django-admin peut être trouvée ici :
`<https://docs.djangoproject.com/fr/3.2/ref/contrib/admin/>`_.
