Member - Gestion des adhérents 
==============================


Modèles
-------

``Profile``: un objet polymorphique pour représenter soit une personne physique
soit une organisation de personnes.

``Person``: une personne physique, éventuellement avec une adhésion en cours de 
validité

``Organization``: un groupe de personnes représentées par un gérant de l'organisation

``Membership``: une adhésion associée à une personne et une date de validité.
Ce modèle est une table de cache.


Gestion des groupes (``Organization``)
--------------------------------------

Les organisations permettent de donner accès à des produits ou privilèges sur la base de
l'appartenance à un groupe.

La création de groupe se fait via Django-admin. Elle nécessite le privilège "création d'Organisation".
Si vous ne l'avez pas contactez un administrateur.

Accéder à la page ``User``, créer un utilisateur (renseigner les identifiants du club, une addresse),
puis cliquer sur "+ Ajouter un profil" et sélectionner "Organization".

Il suffit ensuite de désigner le représentant de l'organisation parmi les adhérents et d'ajouter
les membres.

Mettre à jour la liste des membres se fait dans l'interface de gestion de compte du représentant
de l'association, les autres modifications doivent êtres faites par un administrateur.


Certains des produits ne sont accessibles que pour les groupes, plus de précisions sont données
dans la page `Billing <billing.html>`_



Sémantique des adhésions (``Membership``)
-----------------------------------------

Une adhésion est valide à partir de la date de validation de la facture qui lui est associée,
sauf si une autre adhésion est déjà en cours de validité. Dans ce cas la date de début de l'adhésion
est reportée à la fin de la période de validité de l'adhésion en cours.

En particulier :

- il est impossible que plusieurs adhésions pour un seul utilisateur aient des périodes de validité
  non disjointes
- la somme totale des durées des périodes de validité dépend uniquement des types d'adhésions achetées,
  et est indépendante de leur ordre ou date d'achat

Les périodes disjointes de validité des adhésions sont recalculées de manière déterministe à chaque
mise à jour ou ajout d'une facture pour une adhésion associée à une personne donnée.
Des tables de cache sont générées dans ``billing``


